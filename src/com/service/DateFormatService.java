package com.service;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DateFormatService {
    public static String formatJJMMYYYY(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }
    
    public static String formatYYYYMMJJ(Date date) {
        return new SimpleDateFormat("yyyy/MM/dd").format(date);
    }
}
