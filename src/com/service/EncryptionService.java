package com.service;

import java.math.BigInteger;
import java.security.MessageDigest;

public class EncryptionService {

    public String encryptMD5(String source) {
        String md5;
        try {
            MessageDigest mdEnc = MessageDigest.getInstance("MD5"); // Encryption algorithm
            mdEnc.update(source.getBytes(), 0, source.length());
            md5 = new BigInteger(1, mdEnc.digest()).toString(16); // Encrypted string
        }
        catch (Exception e) {
            return null;
        }
        return md5;
    }
    
    public String encryptSHA256(String source) {
        String sha256;
        try {
            MessageDigest shaEnc = MessageDigest.getInstance("SHA-256"); // Encryption algorithm
            shaEnc.update(source.getBytes(), 0, source.length());
            sha256 = new BigInteger(1, shaEnc.digest()).toString(16); // Encrypted string
        } catch (Exception e) {
            return null;
        }
        return sha256;
    }
}
