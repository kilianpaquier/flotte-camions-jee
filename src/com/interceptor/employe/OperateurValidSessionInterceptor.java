package com.interceptor.employe;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.util.Map;

public class OperateurValidSessionInterceptor extends AbstractInterceptor {

    public String intercept(ActionInvocation invocation) throws Exception {
        Map<String, Object> session = invocation.getInvocationContext().getSession();

        if (!session.containsKey("operateurID")) {
            return "connexion";
        }

        return invocation.invoke();
    }
}
