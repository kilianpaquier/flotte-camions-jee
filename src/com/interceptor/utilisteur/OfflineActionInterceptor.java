package com.interceptor.utilisteur;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.util.Map;

public class OfflineActionInterceptor extends AbstractInterceptor {

    public String intercept(ActionInvocation invocation) throws Exception {
        Map<String, Object> session = invocation.getInvocationContext().getSession();

        if (session.containsKey("compteID")) {
            return "index"; // TODO: "Il faut être déconecté pour pouvoir accéder à cette page"
        }

        return invocation.invoke();
    }
}