package com.interceptor.client;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.util.Map;

public class ClientValidSessionInterceptor extends AbstractInterceptor {

    public String intercept(ActionInvocation invocation) throws Exception {
        Map<String, Object> session = invocation.getInvocationContext().getSession();

        if (!session.containsKey("clientID")) {
            return "connexion";
        }

        return invocation.invoke();
    }
}
