package com.action.camion;

import com.dao.camion.CamionDAO;
import com.model.camion.Camion;
import com.model.camion.EtatCamion;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CamionAction extends ActionSupport {
    
    private final CamionDAO camionDAO = new CamionDAO();
    
    @Getter
    @Setter
    private Camion camion;
    
    @Getter
    @Setter
    private String etatCamion;
    
    @Getter
    @Setter
    private List<Camion> camions = new ArrayList<>();
    
    @Getter
    @Setter
    private List<EtatCamion> etatsCamion = Arrays.asList(EtatCamion.values());
    
    @SkipValidation
    public String index() {
        return SUCCESS;
    }
    
    public void refreshList() {
        camions = camionDAO.getList();
    }
    
    public String createCamion() {
        refreshList();
        try {
            camion = camionDAO.create(camion);
            addActionMessage("Camion ajouté avec succès !");
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
    
    @SkipValidation
    public String readCamion() {
        try {
            camion = camionDAO.get(camion.getIdCamion());
            etatCamion = camion.getEtatCamion().getValeur();
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
    
    public String updateCamion() {
        refreshList();
        try {
            camion = camionDAO.update(camion.getIdCamion(), camion);
            addActionMessage("Camion mis à jour avec succès!");
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
    
    @SkipValidation
    public String deleteCamion() {
        refreshList();
        try {
            camionDAO.remove(camion.getIdCamion());
            addActionMessage("Camion supprimé avec succès!");
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
    
    @SkipValidation
    public String listCamion() {
        refreshList();
        return SUCCESS;
    }
    
    @Override
    public void validate() {
        camion.setEtatCamion(EtatCamion.parse(etatCamion));
        
        if (camion.getImmatriculation().length() == 0) {
            addFieldError("camion.immatriculation", "Veuillez saisir l'immatriculation");
        }
        if (camion.getPrixJournalier() < 0) {
            addFieldError("camion.prixJournalier", "Veuillez saisir un prix valide");
        }
        if (camion.getCapacite() < 0) {
            addFieldError("camion.capacite", "Veuillez saisir une capacité valide (nombre)");
        }
        if (camion.getEtatCamion() == null) {
            addFieldError("camion.etatCamion", "Veuillez saisir l'état du camion");
        }
    }
}
