package com.action.camion;

import com.dao.camion.RemorqueDAO;
import com.model.camion.Remorque;
import com.model.marchandise.TypeMarchandise;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class RemorqueAction extends ActionSupport {
    
    private final RemorqueDAO remorqueDAO = new RemorqueDAO();
    
    private List<Remorque> remorques = new ArrayList<>();
    private Remorque remorque;
    private List<TypeMarchandise> typeMarchandises = Arrays.asList(TypeMarchandise.values());
    private String typeMarchandise;
    
    public static void staticValidation(ActionSupport actionSupport, Remorque remorque, String embeddingPath) {
        if (remorque.getImmatriculation().length() == 0)
            actionSupport.addFieldError(embeddingPath + "remorque.immatriculation", "Veuillez saisir une immatriculation");
    }
    
    @SkipValidation
    public String initList() {
        remorques = remorqueDAO.getList();
        return SUCCESS;
    }
    
    public String addRemorque() {
        initList();
        try {
            remorqueDAO.create(remorque);
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Override
    public void validate() {
        remorque.setTypeMarchandise(TypeMarchandise.parse(typeMarchandise));
        staticValidation(this, remorque, "");
    }
    
    @SkipValidation
    public String modifierRemorque() {
        initList();
        try {
            remorque = remorqueDAO.get(remorque.getIdRemorque());
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String validerModification() {
        try {
            initList();
            remorqueDAO.update(remorque.getIdRemorque(), remorque);
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String supprimerRemorque() {
        initList();
        try {
            remorqueDAO.remove(remorque.getIdRemorque());
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
}
