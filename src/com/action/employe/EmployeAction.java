package com.action.employe;

import com.dao.employe.ConducteurDAO;
import com.dao.employe.OperateurDAO;
import com.dao.employe.TechnicienDAO;
import com.model.compte.TypeCompte;
import com.model.employe.Competence;
import com.model.employe.Conducteur;
import com.model.employe.Operateur;
import com.model.employe.Technicien;
import com.opensymphony.xwork2.ActionSupport;
import com.service.EncryptionService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class EmployeAction extends ActionSupport implements SessionAware {
    
    private Map<String, Object> userSession;
    
    private final ConducteurDAO conducteurDAO = new ConducteurDAO();
    private final OperateurDAO operateurDAO = new OperateurDAO();
    private final TechnicienDAO technicienDAO = new TechnicienDAO();
    
    private final EncryptionService encryptionService = new EncryptionService();
    
    private List<Conducteur> conducteurs = new ArrayList<>();
    private List<Operateur> operateurs = new ArrayList<>();
    private List<Technicien> techniciens = new ArrayList<>();
    private List<Competence> competences = new ArrayList<>();
    
    private Conducteur conducteur;
    private Operateur operateur;
    private Technicien technicien;
    private String competence;
    
    @SkipValidation
    public String initPage() {
        conducteurs = conducteurDAO.getList();
        operateurs = operateurDAO.getList();
        techniciens = technicienDAO.getList();
        competences = Arrays.asList(Competence.values());
        return SUCCESS;
    }
    
    @SkipValidation
    public String addOperateur() {
        try {
            initPage();
            operateur.setDisponible(true);
            operateur.getCompte().setTypeCompte(TypeCompte.OPERATEUR);
            operateur.getCompte().setMotDePasse(encryptionService.encryptMD5(operateur.getCompte().getMotDePasse()));
            operateurDAO.create(operateur);
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String deleteOperateur() {
        initPage();
        try {
            operateurDAO.remove(operateur.getIdEmploye());
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String modifierOperateur() {
        initPage();
        try {
            operateur = operateurDAO.get(operateur.getIdEmploye());
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String validerModifierOperateur() {
        try {
            initPage();
            operateur.setDisponible(true);
            operateur.getCompte().setTypeCompte(TypeCompte.OPERATEUR);
            operateur.getCompte().setMotDePasse(encryptionService.encryptMD5(operateur.getCompte().getMotDePasse()));
            operateurDAO.update(operateur.getIdEmploye(), operateur);
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String addConducteur() {
        try {
            initPage();
            conducteur.setDisponible(true);
            conducteurDAO.create(conducteur);
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String deleteConducteur() {
        initPage();
        try {
            conducteurDAO.remove(conducteur.getIdEmploye());
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String modifierConducteur() {
        initPage();
        try {
            conducteur = conducteurDAO.get(conducteur.getIdEmploye());
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String validerModifierConducteur() {
        try {
            initPage();
            conducteur.setDisponible(true);
            conducteurDAO.update(conducteur.getIdEmploye(), conducteur);
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String deleteTechnicien() {
        initPage();
        try {
            technicienDAO.remove(technicien.getIdEmploye());
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String modifierTechnicien() {
        initPage();
        try {
            technicien = technicienDAO.get(technicien.getIdEmploye());
            userSession.put("technicien", technicien);
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
    
    @SkipValidation
    public String addCompetence() {
        initPage();
        technicien = (Technicien) userSession.get("technicien");
        if (technicien == null)
            technicien = new Technicien();
        technicien.addCompetence(Competence.parse(competence));
        userSession.put("technicien", technicien);
        return SUCCESS;
    }
    
    public String createTechnicien() {
        Technicien tempTechnicien = (Technicien) userSession.get("technicien");
        technicien.setCompetences(tempTechnicien.getCompetences());
        technicien.setDisponible(true);
        try {
            technicienDAO.create(technicien);
            userSession.put("technicien", null);
            return SUCCESS;
        } catch (SQLException e) {
           addActionError(e.getMessage());
           return ERROR;
        }
    }
    
    @Override
    public void setSession(Map<String, Object> map) {
        userSession = map;
    }
    
    public String validerModifierTechnicien() {
        try {
            Technicien tempTechnicien = (Technicien) userSession.get("technicien");
            technicien.setCompetences(tempTechnicien.getCompetences());
            technicien.setDisponible(true);
            technicienDAO.update(technicien.getIdEmploye(), technicien);
            userSession.put("technicien", null);
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
}
