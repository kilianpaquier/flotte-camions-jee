package com.action.client;

import com.action.commun.AdresseAction;
import com.action.compte.CompteAction;
import com.dao.client.ClientDAO;
import com.model.client.Client;
import com.model.client.Fidelite;
import com.model.compte.Compte;
import com.model.compte.TypeCompte;
import com.opensymphony.xwork2.ActionSupport;
import com.service.EncryptionService;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.Map;

public class ClientAction extends ActionSupport implements SessionAware {
    
    private final EncryptionService encryptionService = new EncryptionService();
    private final ClientDAO clientDAO = new ClientDAO();
    
    @Getter
    private Map<String, Object> userSession;
    
    @Getter
    @Setter
    private Client client;
    
    @Getter
    @Setter
    private Compte compte;
    
    @Override
    public void setSession(Map<String, Object> session) {
        userSession = session;
    }
    
    @SkipValidation
    public String index() {
        return SUCCESS;
    }
    
    public String createClient() {
        client.getCompte().setTypeCompte(TypeCompte.CLIENT);
        client.setFidelite(Fidelite.BRONZE);
        client.getCompte().setMotDePasse(encryptionService.encryptMD5(client.getCompte().getMotDePasse()));
        
        try {
            client = clientDAO.create(client);
            compte = client.getCompte();
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        
        addActionMessage("Compte créé avec succès !");
        return SUCCESS;
    }
    
    @SkipValidation
    public String readClient() {
        int clientID = (int) userSession.get("clientID");
        try {
            client = clientDAO.get(clientID);
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String updateClient() {
        try {
            int clientID = (int) userSession.get("clientID");
            Client tempClient = clientDAO.get(clientID); // Pour récupérer la fidélité, le numéro de l'adresse et le numéro de compte
            client.setFidelite(tempClient.getFidelite());
            client.getCompte().setIdCompte(tempClient.getCompte().getIdCompte());
            client.getAdresse().setIdAdresse(tempClient.getAdresse().getIdAdresse());
            
            client.getCompte().setTypeCompte(TypeCompte.CLIENT);
            client.getCompte().setMotDePasse(encryptionService.encryptMD5(client.getCompte().getMotDePasse()));
            client = clientDAO.update(clientID, client);
            compte = client.getCompte();
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
        addActionMessage("Compte mis à jour avec succès!");
        return SUCCESS;
    }
    
    @SkipValidation
    public String deleteClient() throws Exception {
        int clientID = (int) userSession.get("clientID");
        if (clientDAO.remove(clientID)) {
            addActionMessage("Compte supprimé avec succès !");
            return SUCCESS;
        }
        addActionError("Erreur lors de la suppression du compte");
        return ERROR;
    }
    
    @Override
    public void validate() {
        /* Client */
        if (client.getPrenom().length() == 0) {
            addFieldError("client.prenom", "Veuillez saisir un prénom.");
        }
        if (client.getNom().length() == 0) {
            addFieldError("client.nom", "Veuillez saisir un nom.");
        }
        if (client.getTelephone().length() == 0) {
            addFieldError("client.telephone", "Veuillez saisir un numéro de téléphone.");
        }
        
        /* Client.Adresse */
        AdresseAction.staticValidation(this, client.getAdresse(), "client.");
        
        /* Client.Compte */
        CompteAction.staticValidation(this, client.getCompte(), "client.");
    }
}
