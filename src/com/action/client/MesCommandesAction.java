package com.action.client;

import com.dao.commande.CommandeDAO;
import com.dao.livraison.LivraisonDAO;
import com.model.commande.Commande;
import com.model.commande.SousCommande;
import com.model.livraison.Livraison;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.struts2.interceptor.SessionAware;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class MesCommandesAction extends ActionSupport implements SessionAware {
    
    private final CommandeDAO commandeDAO = new CommandeDAO();
    private final LivraisonDAO livraisonDAO = new LivraisonDAO();
    
    private Map<String, Object> userSession;
    private List<Commande> commandes = new ArrayList<>();
    private List<Livraison> livraisons = new ArrayList<>();
    
    public String initPage() {
        commandes = commandeDAO.getByClient((int) userSession.get("clientID"));
        for (Commande commande : commandes) {
            for (SousCommande sousCommande : commande.getSousCommandes()) {
                try {
                    livraisons.add(livraisonDAO.getBySousCommande(sousCommande.getIdSousCommande()));
                } catch (SQLException ignored) {
                
                }
            }
        }
        return SUCCESS;
    }
    
    @Override
    public void setSession(Map<String, Object> map) {
        userSession = map;
    }
}
