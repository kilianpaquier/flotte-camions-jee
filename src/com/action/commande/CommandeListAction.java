package com.action.commande;

import com.dao.commande.CommandeDAO;
import com.model.commande.Commande;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.util.ArrayList;
import java.util.List;

public class CommandeListAction extends ActionSupport {

    private final CommandeDAO COMMANDE_DAO = new CommandeDAO();

    @Getter
    @Setter
    private List<Commande> commandes = new ArrayList<>();

    public static void staticValidation(ActionSupport action, Commande commande, String embeddingPath) {

    }

    @SkipValidation
    public String initPage() {
        commandes = COMMANDE_DAO.getListWhitoutLivraison();
        return SUCCESS;
    }

    @Override
    public void validate() {
        super.validate();
    }
}
