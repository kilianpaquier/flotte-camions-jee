package com.action.commande;

import com.dao.client.ClientDAO;
import com.dao.commande.CommandeDAO;
import com.model.commande.Commande;
import com.model.commande.Frequence;
import com.model.commande.SousCommande;
import com.model.marchandise.Marchandise;
import com.model.marchandise.TypeMarchandise;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.time.Instant;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
@Data
public class CommandeAction extends ActionSupport implements SessionAware {
    
    private final CommandeDAO COMMANDE_DAO = new CommandeDAO();
    private final ClientDAO CLIENT_DAO = new ClientDAO();
    
    private Map<String, Object> userSession;
    private Commande commande;
    private String frequence;
    private String typeMarchandise;
    private Marchandise marchandise;
    
    private SousCommande sousCommande;
    // Edition d'une sous commande puis ajout dans la commande (réduire la complexité de gestion de la page)
    
    private List<Marchandise> marchandisesList = new ArrayList<>();
    // Pour la datalist des marchandises -- Unused for now
    
    private List<Frequence> frequences = new ArrayList<>();
    // Pour la combobox des fréquences
    
    private List<TypeMarchandise> typeMarchandises = new ArrayList<>();
    // Pour la combobox des types de marchandise
    
    public static void staticValidation(ActionSupport action, Commande commande, String embeddingPath) {
        if (commande == null || commande.getSousCommandes().size() == 0) {
            action.addActionError("Votre commande est vide");
        }
    }
    
    public static void staticValidation(ActionSupport action, SousCommande sousCommande, String embeddingPath) {
        if (sousCommande.getMarchandises().size() == 0)
            action.addActionError("Votre sous commande ne possède pas de marchandise");
        if (sousCommande.getFrequence() == null)
            action.addFieldError(embeddingPath + "frequence", "Vous n'avez pas sélectionné de fréquence");
        if (sousCommande.getDateDepart() == null)
            action.addFieldError(embeddingPath + "sousCommande.dateDepart", "Vous n'avez pas donné de date de départ");
        if (sousCommande.getLieuDepart().getRue().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuDepart.rue", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getLieuDepart().getVille().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuDepart.ville", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getLieuDepart().getPays().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuDepart.pays", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getLieuDepart().getCodePostal().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuDepart.codePostal", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getLieuArrivee().getRue().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuArrivee.rue", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getLieuArrivee().getVille().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuArrivee.ville", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getLieuArrivee().getPays().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuArrivee.pays", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getLieuArrivee().getCodePostal().length() == 0)
            action.addFieldError(embeddingPath + "sousCommande.lieuArrivee.codePostal", "Vous n'avez pas saisi de lieu de départ");
        if (sousCommande.getDateDepart().before(Date.from(Instant.now())))
            action.addFieldError(embeddingPath + "sousCommande.dateDepart", "La date de départ ne peux être antérieure à aujourd'hui");
    }
    
    public static void staticValidation(ActionSupport action, Marchandise marchandise, String embeddingPath) {
        if (marchandise.getVolume() == 0)
            action.addFieldError(embeddingPath + "marchandise.volume", "Vous n'avez pas saisi de volume pour la marchandise");
        if (marchandise.getTypeMarchandise() == null)
            action.addFieldError(embeddingPath + "typeMarchandise", "Vous n'avez pas saisi le type de la marchandise");
        if (marchandise.getNom().length() == 0)
            action.addFieldError(embeddingPath + "marchandise.nom", "Vous n'avez pas saisi le nom de la marchandise");
    }
    
    @SkipValidation
    public String order() throws SQLException {
        initPage();
        commande = (Commande) userSession.get("commande");
        commande.setDateCommande(Date.from(Instant.now()));
        commande.setClient(CLIENT_DAO.get((int) userSession.get("clientID")));
        
        staticValidation(this, commande, "");
        if (hasActionErrors() || hasFieldErrors())
            return ERROR;
        
        try {
            COMMANDE_DAO.create(commande);
            userSession.put("commande", null);
        } catch (SQLException e) {
            addActionError("Erreur lors de la création de la commande");
            return ERROR;
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String initPage() {
        try {
            frequences.addAll(Arrays.asList(Frequence.values()));
            typeMarchandises.addAll(Arrays.asList(TypeMarchandise.values()));
            if (commande == null)
                commande = new Commande();
            commande.setClient(CLIENT_DAO.get((int) userSession.get("clientID")));
        } catch (SQLException e) {
            addActionError("Impossible de récupérer votre numéro de client");
        }
        return SUCCESS;
    }
    
    @SkipValidation
    public String addSousCommande() {
        commande = (Commande) userSession.get("commande");
        initPage();
        SousCommande sousCommande = (SousCommande) userSession.get("sousCommande");
        
        if (sousCommande == null) {
            addActionError("Votre sous commande ne possède pas de marchandise");
            return ERROR;
        }
        if (commande == null) {
            commande = new Commande();
        }
        
        this.sousCommande.setFrequence(Frequence.parse(frequence));
        this.sousCommande.setMarchandises(sousCommande.getMarchandises());
        staticValidation(this, this.sousCommande, "");
        if (hasFieldErrors() || hasActionErrors())
            return ERROR;
        
        commande.addSousCommande(this.sousCommande);
        userSession.put("commande", commande);
        userSession.put("sousCommande", null);
        this.sousCommande = new SousCommande();
        return SUCCESS;
    }
    
    @SkipValidation
    public String addMarchandise() {
        initPage();
        
        sousCommande = userSession.get("sousCommande") != null ? (SousCommande) userSession.get("sousCommande") : new SousCommande();
        marchandise.setTypeMarchandise(TypeMarchandise.parse(typeMarchandise));
        
        staticValidation(this, marchandise, "");
        if (hasFieldErrors() || hasActionErrors())
            return ERROR;
        
        sousCommande.addMarchandise(marchandise);
        userSession.put("sousCommande", sousCommande);
        marchandise = new Marchandise();
        return SUCCESS;
    }
    
    @Override
    public void validate() {
        staticValidation(this, commande, "");
    }
    
    @Override
    public void setSession(Map<String, Object> map) {
        userSession = map;
    }
    
    @SkipValidation
    public String delete() {
        initPage();
        userSession.put("sousCommande", null);
        userSession.put("commande", null);
        commande = new Commande();
        sousCommande = new SousCommande();
        marchandise = new Marchandise();
        addActionMessage("Commande supprimée");
        return SUCCESS;
    }
}
