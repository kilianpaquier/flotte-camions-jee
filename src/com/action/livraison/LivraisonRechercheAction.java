package com.action.livraison;

import com.dao.livraison.LivraisonDAO;
import com.model.commande.EtatLivraison;
import com.model.livraison.Livraison;
import com.model.marchandise.TypeMarchandise;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter @Setter
public class LivraisonRechercheAction extends ActionSupport {

    private final LivraisonDAO LIVRAISON_DAO = new LivraisonDAO();

    private String nomClient;
    private Date dateCommande;
    private String immatriculationRemorque;
    private String immatriculationCamion;
    private String nomConducteur;
    private Date dateDepart;
    private Date dateArrivee;
    private String villeDepart;
    private String villeArrivee;
    private String typeMarchandise;
    private String etatLivraison;

    private List<Livraison> livraisons = new ArrayList<>();
    private List<String> etatLivraisons = new ArrayList<>();
    private List<String> typeMarchandises = new ArrayList<>();

    @SkipValidation
    public String initPage() {
        typeMarchandises.add("Toutes marchandises");
        for (TypeMarchandise typeMarchandise : TypeMarchandise.values())
            typeMarchandises.add(typeMarchandise.getValeur());

        etatLivraisons.add("Tous états");
        for (EtatLivraison etatLivraison : EtatLivraison.values())
            etatLivraisons.add(etatLivraison.getValeur());
        return SUCCESS;
    }

    @SkipValidation
    public String search() {
        initPage();

        try {
            livraisons = LIVRAISON_DAO.search(
                    nomClient, dateCommande,
                    immatriculationCamion, immatriculationRemorque, nomConducteur, typeMarchandise,
                    villeDepart, villeArrivee, dateDepart, dateArrivee, etatLivraison);

            return SUCCESS;
        }
        catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }

    @Override
    public void validate() {
        super.validate();
    }
}
