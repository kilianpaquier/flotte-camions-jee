package com.action.livraison;

import com.dao.camion.CamionDAO;
import com.dao.camion.RemorqueDAO;
import com.dao.commande.SousCommandeDAO;
import com.dao.employe.ConducteurDAO;
import com.dao.livraison.LivraisonDAO;
import com.model.camion.Camion;
import com.model.camion.Remorque;
import com.model.commande.EtatLivraison;
import com.model.livraison.Itineraire;
import com.model.livraison.Livraison;
import com.model.employe.Conducteur;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class LivraisonValidationAction extends ActionSupport {
    
    private final LivraisonDAO livraisonDAO = new LivraisonDAO();
    private final SousCommandeDAO sousCommandeDAO = new SousCommandeDAO();
    private final CamionDAO camionDAO = new CamionDAO();
    private final ConducteurDAO conducteurDAO = new ConducteurDAO();
    private final RemorqueDAO remorqueDAO = new RemorqueDAO();
    
    private Livraison livraison;
    private List<Camion> camions = new ArrayList<>();
    private List<Conducteur> conducteurs = new ArrayList<>();
    private List<Remorque> remorques = new ArrayList<>();
    
    private int idSousCommande;
    private String conducteur;
    private String remorque;
    private String camion;
    
    public static void staticValidation(ActionSupport action, Livraison livraison, String embeddingPath) {
        if (livraison.getConducteur() == null)
            action.addFieldError(embeddingPath + "livraison.conducteur", "Veuillez sélectionner un conducteur");
        if (livraison.getCamion() == null)
            action.addFieldError(embeddingPath + "livraison.camion", "Veuillez sélectionner un camion");
        if (livraison.getRemorque() == null)
            action.addFieldError(embeddingPath + "livraison.remorque", "Veuillez sélectionner une remorque");
        if (livraison.getDateDepart() == null)
            action.addFieldError(embeddingPath + "livraison.dateDepart", "Veuillez choisir une date de départ");
        if (livraison.getItineraire() == null)
            action.addFieldError(embeddingPath + "livraison.itineraire", "Veuillez calculer un itinéraire");
    }
    
    @SkipValidation
    public String validOrder() {
        try {
            initPage();
            livraison.setSousCommande(sousCommandeDAO.get(livraison.getSousCommande().getIdSousCommande()));
            livraison.setCamion(parseCamion(camion));
            livraison.setRemorque(parseRemorque(remorque));
            livraison.setConducteur(parseConducteur(conducteur));
            livraison.setEtatLivraison(EtatLivraison.EN_PREPARATION);
            Map<String, Object> itineraire = Itineraire.createItiniraire(livraison.getDateDepart(), livraison.getSousCommande().getLieuDepart(), livraison.getSousCommande().getLieuArrivee());
            livraison.setDateArrivee((Date) itineraire.get("Arrivée"));
            livraison.setItineraire((Itineraire) itineraire.get("Itineraire"));
            
            staticValidation(this, livraison, "");
            if (hasActionErrors() || hasFieldErrors())
                return ERROR;
            livraisonDAO.create(livraison);
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
    
    @SkipValidation
    public String initPage() {
        try {
            if (livraison == null)
                livraison = new Livraison();
            livraison.setSousCommande(sousCommandeDAO.get(idSousCommande));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        camions = camionDAO.getList();
        conducteurs = conducteurDAO.getList();
        remorques = remorqueDAO.getList();
        return SUCCESS;
    }
    
    private Camion parseCamion(String toString) {
        String immat = toString.split("-")[0].trim();
        for (Camion camion : camions) {
            if (camion.getImmatriculation().equals(immat))
                return camion;
        }
        return null;
    }
    
    private Remorque parseRemorque(String toString) {
        String immat = toString.split("-")[0].trim();
        for (Remorque remorque : remorques) {
            if (remorque.getImmatriculation().equals(immat))
                return remorque;
        }
        return null;
    }
    
    private Conducteur parseConducteur(String toString) {
        int id = Integer.parseInt(toString.split(":")[0].trim());
        for (Conducteur conducteur : conducteurs) {
            if (conducteur.getIdEmploye() == id)
                return conducteur;
        }
        return null;
    }
    
    @SkipValidation
    public String calculItineraire() {
        try {
            livraison.setSousCommande(sousCommandeDAO.get(livraison.getSousCommande().getIdSousCommande()));
            Map<String, Object> itineraire = Itineraire.createItiniraire(livraison.getDateDepart(), livraison.getSousCommande().getLieuDepart(), livraison.getSousCommande().getLieuArrivee());
            livraison.setDateArrivee((Date) itineraire.get("Arrivée"));
            livraison.setItineraire((Itineraire) itineraire.get("Itineraire"));
            
            initPage();
            livraison.setCamion(parseCamion(camion));
            livraison.setRemorque(parseRemorque(remorque));
            livraison.setConducteur(parseConducteur(conducteur));
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
    
    @Override
    public void validate() {
        staticValidation(this, livraison, "");
    }
}
