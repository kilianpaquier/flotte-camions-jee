package com.action.livraison;

import com.dao.commande.CommandeDAO;
import com.dao.livraison.LivraisonDAO;
import com.model.commande.SousCommande;
import com.model.livraison.Livraison;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LivraisonListAction extends ActionSupport {
    
    private final LivraisonDAO livraisonDAO = new LivraisonDAO();
    private final CommandeDAO commandeDAO = new CommandeDAO();
    
    @Getter
    @Setter
    private List<Livraison> livraisons = new ArrayList<>();
    
    @Getter
    @Setter
    private int idCommande;
    
    @SkipValidation
    public String initPage() {
        livraisons = livraisonDAO.getList();
        return SUCCESS;
    }
    
    @Override
    public void validate() {
        super.validate();
    }
    
    @SkipValidation
    public String simpleSearch() {
        try {
            if (idCommande == 0)
                livraisons = livraisonDAO.getList();
            else {
                for (SousCommande sousCommande : commandeDAO.get(idCommande).getSousCommandes()) {
                    livraisons.add(livraisonDAO.getBySousCommande(sousCommande.getIdSousCommande()));
                }
            }
            return SUCCESS;
        } catch (SQLException e) {
            addActionError(e.getMessage());
            return ERROR;
        }
    }
}
