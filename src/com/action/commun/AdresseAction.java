package com.action.commun;

import com.model.commun.Adresse;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;

public class AdresseAction extends ActionSupport {

    @Getter
    @Setter
    private Adresse adresse;

    public static void staticValidation(ActionSupport action, Adresse adresse, String embeddingPath) {
        if (adresse.getCodePostal().length() == 0) {
            action.addFieldError(embeddingPath + "adresse.codePostal", "Veuillez saisir un code postal.");
        }
        if (adresse.getPays().length() == 0) {
            action.addFieldError(embeddingPath + "adresse.pays", "Veuillez saisir un pays.");
        }
        if (adresse.getRue().length() == 0) {
            action.addFieldError(embeddingPath + "adresse.rue", "Veuillez saisir une rue.");
        }
        if (adresse.getVille().length() == 0) {
            action.addFieldError(embeddingPath + "adresse.ville", "Veuillez saisir une ville.");
        }
    }

    @Override
    public void validate() {
        AdresseAction.staticValidation(this, adresse, "");
    }
}
