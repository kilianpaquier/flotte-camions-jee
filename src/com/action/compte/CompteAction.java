package com.action.compte;

import com.dao.compte.CompteDAO;
import com.model.compte.Compte;
import com.opensymphony.xwork2.ActionSupport;
import com.service.EncryptionService;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.interceptor.validation.SkipValidation;

public class CompteAction extends ActionSupport {

    private final CompteDAO compteDAO = new CompteDAO();

    private final EncryptionService encryptionService = new EncryptionService();

    @Getter
    @Setter
    private Compte compte;

    public static void staticValidation(ActionSupport action, Compte compte, String embeddingPath) {
        if (compte.getEmail().length() == 0) {
            action.addFieldError(embeddingPath + "compte.email", "Veuillez saisir une adresse email.");
        }
        if (compte.getMotDePasse().length() == 0) {
            action.addFieldError(embeddingPath + "compte.motDePasse", "Veuillez saisir un mot de passe.");
        }
    }

    @Override
    public void validate() {
        CompteAction.staticValidation(this, compte, "");
    }

    @SkipValidation
    public String index() {
        return SUCCESS;
    }

    public String createCompte() throws Exception {
        //TODO Vérifier l'existence de l'adresse mail et le double mdp (opt.)
        compte.setMotDePasse(
                encryptionService.encryptMD5(compte.getMotDePasse())
        );
        compteDAO.create(compte);
        return SUCCESS;
    }

    public String readCompte(int id) throws Exception {
        compte = compteDAO.get(id);
        return SUCCESS;
    }

    public String updateCompte() throws Exception {
        compteDAO.update(compte.getIdCompte(), compte);
        return SUCCESS;
    }

    public String deleteCompte(int id) throws Exception {
        compteDAO.remove(id);
        return SUCCESS;
    }
}
