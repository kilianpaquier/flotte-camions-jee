package com.action.compte;

import com.dao.client.ClientDAO;
import com.dao.employe.OperateurDAO;
import com.dao.compte.CompteDAO;
import com.model.client.Client;
import com.model.employe.Operateur;
import com.model.compte.Compte;
import com.opensymphony.xwork2.ActionSupport;
import com.service.EncryptionService;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ConnexionAction extends ActionSupport implements SessionAware {

    private final CompteDAO compteDAO = new CompteDAO();
    private final ClientDAO clientDAO = new ClientDAO();
    private final OperateurDAO operateurDAO = new OperateurDAO();

    private final EncryptionService encryptionService = new EncryptionService();

    @Getter
    private Map<String, Object> userSession;

    @Getter
    @Setter
    private Compte compte;

    @Override
    public void setSession(Map<String, Object> session) {
        userSession = session;
    }

    @SkipValidation
    public String index() {
        return SUCCESS;
    }

    @SkipValidation
    public String connexion() {
        try {
            // If we are coming from createClient or createOperateur
            // we must retrieve the compte from the session
            if (compte == null) {
                if (userSession.containsKey("clientID")) {
                    int clientID = (int) userSession.get("clientID");
                    setCompte(clientDAO.get(clientID).getCompte());
                } else if (userSession.containsKey("operateurID")) {
                    int operateurID = (int) userSession.get("operateurID");
                    setCompte(operateurDAO.get(operateurID).getCompte());
                }
            }
    
            assert compte != null;
            compte.setMotDePasse(encryptionService.encryptMD5(compte.getMotDePasse()));
            
            if ((compte = compteDAO.getFromEmailAndMotDePasse(compte.getEmail(), compte.getMotDePasse())) != null) {
                if (userSession == null) {
                    userSession = new HashMap<>();
                }
        
                userSession.put("compteID", compte.getIdCompte());
                userSession.put("typeCompte", compte.getTypeCompte());
                userSession.put("email", compte.getEmail());
        
                switch (compte.getTypeCompte()) {
                    case CLIENT:
                        Client client = clientDAO.getByCompte(compte.getIdCompte());
                        userSession.put("clientID", client.getIdClient());
                        break;
                    case OPERATEUR:
                        Operateur operateur = operateurDAO.get(compte.getIdCompte());
                        userSession.put("operateurID", operateur.getIdEmploye());
                        break;
                }
                addActionMessage("Connexion réussie !");
                return SUCCESS;
            }
        } catch (SQLException e) {
            addActionError("Impossible de se connecter, veuillez réessayer.");
            return ERROR;
        }
        addActionError("Impossible de se connecter, veuillez réessayer.");
        return ERROR;
    }

    @SkipValidation
    public String deconnexion() {
        userSession.clear();
        return SUCCESS;
    }
}
