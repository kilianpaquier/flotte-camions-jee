package com.model.employe;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Technicien extends Employe {
    private List<Competence> competences = new ArrayList<>();

    @Builder(builderMethodName = "technicienBuilder")
    public Technicien(List<Competence> competences, int id, String nom, String prenom, boolean disponible, String ville) {
        super(id, nom, prenom, disponible, ville);
        this.competences = competences;
    }
    
    public void addCompetence(Competence competence) {
        boolean exists = false;
        for (Competence competence1 : competences) {
            if (competence1.equals(competence))
                exists = true;
        }
        if (!exists)
            competences.add(competence);
    }

    public void parseCompetences(String competences) {
        String[] values = competences.split(";");
        for (String value : values)
            this.competences.add(Competence.parse(value));
    }
    
    public String getCompetencesAsString() {
        StringBuilder value = new StringBuilder();
        for (Competence competence : competences)
            value.append(competence.toString()).append(";");
        return value.toString();
    }
}
