package com.model.employe;

import com.model.compte.Compte;
import lombok.*;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Operateur extends Employe {
    private Compte compte;

    @Builder(builderMethodName = "operateurBuilder")
    public Operateur(int id, String nom, String prenom, boolean disponible, String ville, Compte compte) {
        super(id, nom, prenom, disponible, ville);
        this.compte = compte;
    }
}
