package com.model.employe;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
//@AllArgsConstructor

public class Conducteur extends Employe {
    @Override
    public String toString() {
        return super.toString();
    }
}
