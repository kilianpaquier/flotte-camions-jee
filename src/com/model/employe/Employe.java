package com.model.employe;

import com.model.commun.Adresse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Employe {
    private int idEmploye;
    private String nom;
    private String prenom;
    private boolean disponible;
    private String ville;
    
    @Override
    public String toString() {
        return idEmploye + " : " + nom + " " + prenom + " - " + ville;
    }
}
