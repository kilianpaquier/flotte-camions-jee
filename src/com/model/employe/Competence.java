package com.model.employe;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Competence {
    DIAGNOSTIQUER("Diagnostique"),
    ENTRETENIR("Entretien"),
    REPARER("Réparation");
    
    @Getter
    private String valeur;
    
    public static Competence parse(String value) {
        switch (value) {
            case "Diagnostique":
                return DIAGNOSTIQUER;
            case "Entretien":
                return ENTRETENIR;
            case "Réparation":
                return REPARER;
            default:
                throw new IllegalArgumentException("Impossible de parser la valeur");
        }
    }
    
    @Override
    public String toString() {
        return valeur;
    }
}
