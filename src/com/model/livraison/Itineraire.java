package com.model.livraison;

import com.model.commun.Adresse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.temporal.ChronoUnit;
import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Itineraire {
    private List<Adresse> adresses = new ArrayList<>();
    
    public String getAdressesAsStringKey() {
        StringBuilder value = new StringBuilder();
        for (Adresse adresse : adresses) {
            value.append(adresse.getIdAdresse()).append(";");
        }
        return value.toString();
    }
    
    public static Map<String, Object> createItiniraire(Date dateDepart, Adresse depart, Adresse arrivee) {
        Map<String, Object> result = new HashMap<>();
        /*
        On crée l'itinéraire
         */
        Itineraire itineraire = new Itineraire();
        itineraire.adresses.add(depart);
        itineraire.adresses.add(new Adresse("France", "75000", "Paris", "15 rue du bonjour"));
        itineraire.adresses.add(new Adresse("France", "69000", "Lyon", "15 rue du bonjour"));
        itineraire.adresses.add(new Adresse("France", "75000", "Marseille", "15 rue du bonjour"));
        itineraire.adresses.add(new Adresse("France", "13000", "Paris", "15 rue du bonjour"));
        itineraire.adresses.add(new Adresse("France", "34000", "Montpellier", "15 rue du bonjour"));
        itineraire.adresses.add(new Adresse("France", "17000", "Bordeaux", "15 rue du bonjour"));
        itineraire.adresses.add(new Adresse("France", "44000", "Nantes", "15 rue du bonjour"));
        itineraire.adresses.add(arrivee);
        
        /*
        Création de la date d'arrivée
         */
        Date dateArrivee = Date.from(dateDepart.toInstant().plus(5, ChronoUnit.DAYS));
        
        /*
        Création du retour
         */
        result.put("Itineraire", itineraire);
        result.put("Arrivée", dateArrivee);
        return result;
    }
}
