package com.model.livraison;

import com.model.camion.Camion;
import com.model.camion.Remorque;
import com.model.commande.EtatLivraison;
import com.model.commande.SousCommande;
import com.model.employe.Conducteur;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Livraison {
    private int idLivraison;
    private SousCommande sousCommande;
    private Conducteur conducteur;
    private Date dateDepart;
    private Date dateArrivee;
    private Camion camion;
    private Remorque remorque;
    private EtatLivraison etatLivraison;
    private Itineraire itineraire;
}
