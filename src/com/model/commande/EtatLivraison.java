package com.model.commande;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum EtatLivraison {
    EN_PREPARATION("En préparation"),
    EN_COURS("En cours"),
    LIVREE("Livrée");
    
    @Getter
    private String valeur;
    
    public static EtatLivraison parse(String valeur) {
        switch (valeur) {
            case "En préparation":
                return EtatLivraison.EN_PREPARATION;
            case "En cours":
                return EtatLivraison.EN_COURS;
            case "Livrée":
                return EtatLivraison.LIVREE;
            default:
                throw new IllegalArgumentException("Impossible de parser la valeur");
        }
    }
    
    @Override
    public String toString() {
        return valeur;
    }
}
