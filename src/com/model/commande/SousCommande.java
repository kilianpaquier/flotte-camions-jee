package com.model.commande;

import com.model.commun.Adresse;
import com.model.marchandise.Marchandise;
import com.service.DateFormatService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SousCommande implements Comparable<SousCommande> {
    private int idSousCommande;
    private Date dateDepart;
    private double tarif;
    private List<Marchandise> marchandises = new ArrayList<>();
    private Adresse lieuDepart;
    private Adresse lieuArrivee;
    private Frequence frequence;
    
    public String getMarchandisesAsStringKey() {
        StringBuilder value = new StringBuilder();
        for (Marchandise marchandise : marchandises) {
            value.append(marchandise.getIdMarchandise()).append(";");
        }
        return value.toString();
    }
    
    @Override
    public int compareTo(SousCommande sousCommande) {
        if (dateDepart.equals(sousCommande.dateDepart) &&
                lieuDepart.compareTo(sousCommande.lieuDepart) == 0 &&
                lieuArrivee.compareTo(sousCommande.lieuArrivee) == 0 &&
                frequence.getValeur().equals(sousCommande.frequence.getValeur()))
            return 0;
        return 1;
    }
    
    public void addMarchandise(Marchandise marchandise) {
        boolean contenue = false;
        for (Marchandise marchandiseThis : marchandises) {
            if (marchandiseThis.compareTo(marchandise) == 0) {
                contenue = true;
                marchandiseThis.setVolume(marchandiseThis.getVolume() + marchandise.getVolume());
                break;
            }
        }
        if (!contenue)
            marchandises.add(marchandise);
    }
    
    public double countMarchandises() {
        double count = 0;
        for (Marchandise marchandise : marchandises) {
            count += marchandise.getVolume();
        }
        return count;
    }
    
    public String formatDateDepartJJMMYYYY() {
        return DateFormatService.formatJJMMYYYY(dateDepart);
    }
    
    public String formatDateDepartYYYYMMJJ() {
        return DateFormatService.formatYYYYMMJJ(dateDepart);
    }
    
    public boolean removeMarchandiseByName(String marchandiseName) {
        boolean success = false;
        for (Marchandise marchandise : marchandises) {
            if (marchandise.getNom().equals(marchandiseName)) {
                marchandises.remove(marchandise);
                success = true;
                break;
            }
        }
        return success;
    }
}
