package com.model.commande;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Frequence {
    NON_APPLICABLE("N/A"),
    QUOTIDIEN("Quotidien"),
    HEBDOMADAIRE("Hebdomadaire"),
    MENSUEL("Mensuel");
    
    @Getter
    private String valeur;
    
    public static Frequence parse(String valeur) {
        switch (valeur) {
            case "N/A":
                return Frequence.NON_APPLICABLE;
            case "Quotidien":
                return Frequence.QUOTIDIEN;
            case "Hebdomadaire":
                return Frequence.HEBDOMADAIRE;
            case "Mensuel":
                return Frequence.MENSUEL;
            default:
                throw new IllegalArgumentException("Impossible de parser la valeur frequence");
        }
    }
    
    @Override
    public String toString() {
        return valeur;
    }
}
