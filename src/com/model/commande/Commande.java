package com.model.commande;

import com.model.client.Client;
import com.model.marchandise.Marchandise;
import com.service.DateFormatService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Commande {
    private int idCommande;
    private Client client;
    private Date dateCommande;
    private List<SousCommande> sousCommandes = new ArrayList<>();
    
    public String getSousCommandesAsStringKey() {
        StringBuilder value = new StringBuilder();
        for (SousCommande sousCommande : sousCommandes) {
            value.append(sousCommande.getIdSousCommande()).append(";");
        }
        return value.toString();
    }
    
    public void addSousCommande(SousCommande sousCommande) {
        boolean contenue = false;
        for (SousCommande sousCommandeThis : sousCommandes) {
            if (sousCommandeThis.compareTo(sousCommande) == 0) {
                contenue = true;
                for (Marchandise marchandise : sousCommande.getMarchandises()) {
                    sousCommandeThis.addMarchandise(marchandise);
                }
                break;
            }
        }
        if (!contenue)
            sousCommandes.add(sousCommande);
    }
    
    public String getFirstDepartureDate() {
        Date date = Date.from(Instant.MAX);
        for (SousCommande sousCommande : sousCommandes) {
            if (sousCommande.getDateDepart().before(date))
                date = sousCommande.getDateDepart();
        }
        return DateFormatService.formatJJMMYYYY(date);
    }
}
