package com.model.client;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Fidelite {
    BRONZE("Bronze"),
    ARGENT("Argent"),
    OR("Or"),
    PLATINE("Platine");
    
    private String valeur;
    
    @Override
    public String toString() {
        return valeur;
    }
    
    public static Fidelite parse(String valeur) {
        switch (valeur) {
            case "Bronze":
                return BRONZE;
            case "Argent":
                return ARGENT;
            case "Or":
                return OR;
            case "Platine":
                return PLATINE;
            default:
                throw new IllegalArgumentException("Impossible de parser la valeur");
        }
    }
}
