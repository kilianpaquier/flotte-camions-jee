package com.model.client;

import com.model.commun.Adresse;
import com.model.compte.Compte;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Client {
    private int idClient;
    private String entreprise;
    private String nom;
    private String prenom;
    private String telephone;
    private Adresse adresse;
    private Compte compte;
    private Fidelite fidelite;
}
