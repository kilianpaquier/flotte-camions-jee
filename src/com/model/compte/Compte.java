package com.model.compte;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Compte {
    private int idCompte;
    private TypeCompte typeCompte;
    private String email;
    private String motDePasse;
}