package com.model.compte;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public enum TypeCompte {
    CLIENT("Client"),
    OPERATEUR("Opérateur");
    
    @Getter
    @Setter
    private String value;
    
    public static TypeCompte parse(String value) {
        switch (value) {
            case "Client":
                return CLIENT;
            case "Opérateur":
                return OPERATEUR;
            default:
                throw new IllegalArgumentException("Impossible de parser la valeur");
        }
    }
    
    @Override
    public String toString() {
        return value;
    }
}
