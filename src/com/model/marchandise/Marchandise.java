package com.model.marchandise;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Marchandise implements Comparable<Marchandise> {
    private int idMarchandise;
    private TypeMarchandise typeMarchandise;
    private String nom;
    private double volume;
    
    @Override
    public int compareTo(Marchandise marchandise) {
        if (nom.equals(marchandise.nom) && typeMarchandise.getValeur().equals(marchandise.typeMarchandise.getValeur()))
            return 0;
        return 1;
    }
}
