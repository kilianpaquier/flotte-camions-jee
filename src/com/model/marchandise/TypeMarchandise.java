package com.model.marchandise;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TypeMarchandise {
    FRAIS("Frais"),
    FRAGILE("Fragile"),
    CONGELE("Congelé"),
    ANIMAL("Animal"),
    VIANDE("Viande"),
    POISSON("Poisson"),
    ESSENCE("Essence"),
    MATERIEL_CONSTRUCTION("Matériel de construction");
    
    @Getter
    private String valeur;
    
    public static TypeMarchandise parse(String typeMarchandise) {
        switch (typeMarchandise) {
            case "Frais":
                return TypeMarchandise.FRAIS;
            case "Fragile":
                return TypeMarchandise.FRAGILE;
            case "Congelé":
                return TypeMarchandise.CONGELE;
            case "Animal":
                return TypeMarchandise.ANIMAL;
            case "Viande":
                return TypeMarchandise.VIANDE;
            case "Poisson":
                return TypeMarchandise.POISSON;
            case "Essence":
                return TypeMarchandise.ESSENCE;
            case "Matériel de construction":
                return TypeMarchandise.MATERIEL_CONSTRUCTION;
            default:
                throw new IllegalArgumentException("Impossible de parser la valeur typeMarchandise");
        }
    }
    
    @Override
    public String toString() {
        return valeur;
    }
}
