package com.model.commun;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Adresse implements Comparable<Adresse> {
    private int idAdresse;
    private String pays;
    private String codePostal;
    private String ville;
    private String rue;
    
    public Adresse(String pays, String codePostal, String ville, String rue) {
        this.pays = pays;
        this.codePostal = codePostal;
        this.ville = ville;
        this.rue = rue;
    }
    
    @Override
    public int compareTo(Adresse adresse) {
        if (ville.equals(adresse.ville))
            return 0;
        return 1;
    }

    @Override
    public String toString() {
        return String.format("%s, %s %s, %s", rue, codePostal, ville, pays);
    }
}
