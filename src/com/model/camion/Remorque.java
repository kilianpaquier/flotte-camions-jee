package com.model.camion;

import com.model.marchandise.TypeMarchandise;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Remorque {
    private int idRemorque;
    private String immatriculation;
    private double capacite;
    private double prixJournalier;
    private TypeMarchandise typeMarchandise;
    
    @Override
    public String toString() {
        return immatriculation + " - Capacité : " + capacite;
    }
}
