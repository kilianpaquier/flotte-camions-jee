package com.model.camion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Camion {
    private int idCamion;
    private String immatriculation;
    private double prixJournalier;
    private double capacite;
    private EtatCamion etatCamion;
    
    @Override
    public String toString() {
        return immatriculation + " - Capacité : " + capacite;
    }
}
