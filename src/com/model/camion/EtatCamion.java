package com.model.camion;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum EtatCamion {
    MAINTENANCE("En maintenance"),
    PANNE("En panne"),
    EN_FONCTION("En fonction"),
    DISPONIBLE("Disponible");

    @Getter
    private String valeur;
    
    public static EtatCamion parse(String valeur) {

        for (EtatCamion etat : EtatCamion.values()) {
            if (etat.getValeur().equals(valeur)) {
                return etat;
            }
        }

        throw new IllegalArgumentException("Impossible de parser la valeur");
    }
    
    @Override
    public String toString() {
        return valeur;
    }
}
