package com.model.camion;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum EtatMaintenance {
    EN_COURS("En cours"),
    TERMINEE("Terminée");

    @Getter
    private String titre;

    @Override
    public String toString() {
        return titre;
    }
    
    public static EtatMaintenance parse(String value) {
        switch (value) {
            case "En cours":
                return EN_COURS;
            case "Terminée":
                return TERMINEE;
            default:
                throw new IllegalArgumentException("Impossible de parser la valeur");
        }
    }
}
