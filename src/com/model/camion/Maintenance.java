package com.model.camion;

import com.model.employe.Technicien;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Maintenance {
    private int idMaintenance;
    private int duree;
    private String description;
    private Technicien technicien;
    private EtatMaintenance etatMaintenance;
    private Camion camion;
}
