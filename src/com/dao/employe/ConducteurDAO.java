package com.dao.employe;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.model.employe.Conducteur;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConducteurDAO extends BaseDAO<Conducteur> {
    
    
    @Override
    public Conducteur create(Conducteur conducteur) throws SQLException {
        
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO employe (nom, prenom, disponibilite, ville) VALUE (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, conducteur.getNom());
            statement.setString(2, conducteur.getPrenom());
            statement.setBoolean(3, conducteur.isDisponible());
            statement.setString(4, conducteur.getVille());
            statement.execute();
            
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                conducteur.setIdEmploye((int) generatedKeys.getLong(1));
            }
            
            statement = connection.prepareStatement("INSERT INTO conducteur(idEmploye) VALUE (?)");
            statement.setInt(1, conducteur.getIdEmploye());
            statement.execute();
            
            return conducteur;
        }
    }
    
    @Override
    public Conducteur update(int id, Conducteur conducteur) throws SQLException {
        
        
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE employe SET nom = ?, prenom = ?, ville = ?, disponibilite = ? WHERE idEmploye = ?");
            statement.setString(1, conducteur.getNom());
            statement.setString(2, conducteur.getPrenom());
            statement.setString(3, conducteur.getVille());
            statement.setBoolean(4, conducteur.isDisponible());
            statement.setInt(5, conducteur.getIdEmploye());
            statement.execute();
            return conducteur;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM employe WHERE idEmploye = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public Conducteur get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM employe WHERE idEmploye = ?");
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                Conducteur conducteur = new Conducteur();
                conducteur.setIdEmploye(id);
                conducteur.setNom(result.getString("nom"));
                conducteur.setPrenom(result.getString("prenom"));
                conducteur.setDisponible(result.getBoolean("disponibilite"));
                conducteur.setVille(result.getString("ville"));
                return conducteur;
            }
            throw new SQLException("Impossible de récupérer le conducteur");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<Conducteur> getList() {
        List<Conducteur> listConducteur = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM conducteur inner join employe e on conducteur.idEmploye = e.idEmploye");
            ResultSet result = statement.executeQuery();
            
            while (result.next()) {
                Conducteur conducteur = new Conducteur();
                conducteur.setIdEmploye(result.getInt("idEmploye"));
                conducteur.setNom(result.getString("nom"));
                conducteur.setPrenom(result.getString("prenom"));
                conducteur.setDisponible(result.getBoolean("disponibilite"));
                conducteur.setVille(result.getString("ville"));
                listConducteur.add(conducteur);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return listConducteur;
    }
}
