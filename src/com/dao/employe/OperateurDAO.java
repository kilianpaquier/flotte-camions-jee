package com.dao.employe;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.dao.compte.CompteDAO;
import com.model.employe.Operateur;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OperateurDAO extends BaseDAO<Operateur> {
    
    private final CompteDAO compteDAO = new CompteDAO();
    
    @Override
    public Operateur create(Operateur operateur) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            
            PreparedStatement requete = connection.prepareStatement("INSERT INTO employe (nom, prenom, disponibilite, ville) VALUE (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            requete.setString(1, operateur.getNom());
            requete.setString(2, operateur.getPrenom());
            requete.setBoolean(3, operateur.isDisponible());
            requete.setString(4, operateur.getVille());
            requete.execute();
            
            ResultSet generatedKeys = requete.getGeneratedKeys();
            if (generatedKeys.next()) {
                operateur.setIdEmploye((int) generatedKeys.getLong(1));
            }
            
            int compteID = compteDAO.create(operateur.getCompte()).getIdCompte();
            
            requete = connection.prepareStatement("INSERT INTO operateur (idEmploye, compte) VALUE (?, ?)");
            requete.setInt(1, operateur.getIdEmploye());
            requete.setInt(2, compteID);
            requete.execute();
            
            return operateur;
        }
    }
    
    @Override
    public Operateur update(int id, Operateur operateur) throws SQLException {
        operateur.setIdEmploye(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement requete = connection.prepareStatement("UPDATE employe SET nom = ?, prenom = ?, ville = ?, disponibilite = ? WHERE idEmploye = ?");
            requete.setString(1, operateur.getNom());
            requete.setString(2, operateur.getPrenom());
            requete.setString(3, operateur.getVille());
            requete.setBoolean(4, operateur.isDisponible());
            requete.setInt(5, operateur.getIdEmploye());
            requete.execute();
            
            operateur.setCompte(compteDAO.update(operateur.getCompte().getIdCompte(), operateur.getCompte()));
            return operateur;
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement requete = connection.prepareStatement("SELECT compte FROM operateur WHERE idEmploye = ?");
            requete.setInt(1, id);
            ResultSet set = requete.executeQuery();
            if (set.next())
                compteDAO.remove(set.getInt(1));
            requete = connection.prepareStatement("DELETE FROM employe WHERE idEmploye = ?");
            requete.setInt(1, id);
            requete.execute();
            return true;
        }
    }
    
    @Override
    public Operateur get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM employe INNER JOIN operateur o on employe.idEmploye = o.idEmploye WHERE o.idEmploye = ?");
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                Operateur operateur = new Operateur();
                operateur.setIdEmploye(id);
                operateur.setNom(result.getString("nom"));
                operateur.setPrenom(result.getString("prenom"));
                operateur.setDisponible(result.getBoolean("disponibilite"));
                operateur.setVille(result.getString("ville"));
                operateur.setCompte(compteDAO.get(result.getInt("compte")));
                return operateur;
            }
            throw new SQLException("Impossible de récupérer l'opérateur");
        }
    }
    
    @Override
    public List<Operateur> getList() {
        List<Operateur> listOperateur = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM operateur inner join employe e on operateur.idEmploye = e.idEmploye");
            ResultSet result = statement.executeQuery();
            
            while (result.next()) {
                Operateur operateur = new Operateur();
                operateur.setIdEmploye(result.getInt("idEmploye"));
                operateur.setNom(result.getString("nom"));
                operateur.setPrenom(result.getString("prenom"));
                operateur.setDisponible(result.getBoolean("disponibilite"));
                operateur.setVille(result.getString("ville"));
                operateur.setCompte(compteDAO.get(result.getInt("compte")));
                listOperateur.add(operateur);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOperateur;
    }
}
