package com.dao.employe;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.model.employe.Competence;
import com.model.employe.Technicien;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TechnicienDAO extends BaseDAO<Technicien> {
    
    @Override
    public Technicien create(Technicien technicien) throws SQLException {
        
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO employe(nom, prenom, disponibilite, ville) VALUE (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, technicien.getNom());
            statement.setString(2, technicien.getPrenom());
            statement.setBoolean(3, technicien.isDisponible());
            statement.setString(4, technicien.getVille());
            statement.execute();
            
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                technicien.setIdEmploye((int) generatedKeys.getLong(1));
            }
            
            statement = connection.prepareStatement("INSERT INTO technicien (idEmploye, competences) VALUE (?,?)");
            statement.setInt(1, technicien.getIdEmploye());
            statement.setString(2, technicien.getCompetencesAsString());
            statement.execute();
            
            return technicien;
        }
        
    }
    
    @Override
    public Technicien update(int id, Technicien technicien) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE employe SET nom = ?, prenom = ?, ville = ?, disponibilite = ? WHERE idEmploye = ?");
            statement.setString(1, technicien.getNom());
            statement.setString(2, technicien.getPrenom());
            statement.setString(3, technicien.getVille());
            statement.setBoolean(4, technicien.isDisponible());
            statement.setInt(5, technicien.getIdEmploye());
            statement.execute();
            
            statement = connection.prepareStatement("UPDATE technicien SET competences = ? WHERE idEmploye = ?");
            statement.setString(1, technicien.getCompetencesAsString());
            statement.setInt(2, technicien.getIdEmploye());
            return technicien;
        }
    }
    
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM employe WHERE idEmploye = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        }
    }
    
    @Override
    public Technicien get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM employe INNER JOIN technicien t on employe.idEmploye = t.idEmploye WHERE t.idEmploye = ?");
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                Technicien technicien = new Technicien();
                technicien.setIdEmploye(id);
                technicien.setNom(result.getString("nom"));
                technicien.setPrenom(result.getString("prenom"));
                technicien.setDisponible(result.getBoolean("disponibilite"));
                technicien.setVille(result.getString("ville"));
                
                String[] competences = result.getString("competences").split(";");
                for (String competence: competences)
                    technicien.addCompetence(Competence.parse(competence));
                
                return technicien;
            }
            throw new SQLException("Impossible de récupérer le technicien");
        }
    }
    
    @Override
    public List<Technicien> getList() {
        ArrayList<Technicien> listTechnicien = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM technicien inner join employe e on technicien.idEmploye = e.idEmploye");
            ResultSet result = statement.executeQuery();
            
            while (result.next()) {
                Technicien technicien = new Technicien();
                technicien.setIdEmploye(result.getInt("idEmploye"));
                technicien.setNom(result.getString("nom"));
                technicien.setPrenom(result.getString("prenom"));
                technicien.setDisponible(result.getBoolean("disponibilite"));
                technicien.setVille(result.getString("ville"));
    
                String[] competences = result.getString("competences").split(";");
                for (String competence: competences)
                    technicien.addCompetence(Competence.parse(competence));
                
                listTechnicien.add(technicien);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listTechnicien;
    }
}

