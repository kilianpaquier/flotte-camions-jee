package com.dao.livraison;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.dao.camion.CamionDAO;
import com.dao.camion.RemorqueDAO;
import com.dao.commande.SousCommandeDAO;
import com.dao.commun.AdresseDAO;
import com.dao.employe.ConducteurDAO;
import com.model.commande.EtatLivraison;
import com.model.livraison.Itineraire;
import com.model.livraison.Livraison;
import com.model.commun.Adresse;

import java.sql.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class LivraisonDAO extends BaseDAO<Livraison> {
    
    private final SousCommandeDAO SOUS_COMMANDE_DAO = new SousCommandeDAO();
    private final AdresseDAO ADRESSE_DAO = new AdresseDAO();
    private final ConducteurDAO CONDUCTEUR_DAO = new ConducteurDAO();
    private final CamionDAO CAMION_DAO = new CamionDAO();
    private final RemorqueDAO REMORQUE_DAO = new RemorqueDAO();
    
    @Override
    public Livraison create(Livraison livraison) throws SQLException {
        List<Adresse> adresses = new ArrayList<>();
        for (Adresse adresse : livraison.getItineraire().getAdresses()) {
            adresse = ADRESSE_DAO.create(adresse);
            adresses.add(adresse);
        }
        livraison.getItineraire().setAdresses(adresses);
        
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO livraison (dateDepart, dateArrivee, sousCommande, conducteur, camion, remorque, idLivraison, etatLivraison, itiniraire) VALUE (?,?,?,?,?,?,?,?,?)", com.mysql.jdbc.Statement.RETURN_GENERATED_KEYS);
            statement.setDate(1, new java.sql.Date(livraison.getDateDepart().getTime()));
            statement.setDate(2, new java.sql.Date(livraison.getDateArrivee().getTime()));
            statement.setInt(3, livraison.getSousCommande().getIdSousCommande());
            statement.setInt(4, livraison.getConducteur().getIdEmploye());
            statement.setInt(5, livraison.getCamion().getIdCamion());
            statement.setInt(6, livraison.getRemorque().getIdRemorque());
            statement.setInt(7, livraison.getIdLivraison());
            statement.setString(8, livraison.getEtatLivraison().toString());
            statement.setString(9, livraison.getItineraire().getAdressesAsStringKey());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                livraison.setIdLivraison((int)generatedKeys.getLong(1));
            }

            return livraison;
        }
    }
    
    @Override
    public Livraison update(int id, Livraison livraison) throws SQLException {
        livraison.setIdLivraison(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE livraison SET dateDepart = ?, dateArrivee = ?, etatLivraison = ?, itiniraire = ?, etatLivraison = ?, conducteur = ?, sousCommande = ?, camion = ?, remorque = ? WHERE idLivraison = ?");
            statement.setDate(1, new java.sql.Date(livraison.getDateDepart().getTime()));
            statement.setDate(2, new java.sql.Date(livraison.getDateArrivee().getTime()));
            statement.setString(3, livraison.getItineraire().getAdressesAsStringKey());
            statement.setInt(4, livraison.getIdLivraison());
            statement.setString(5, livraison.getEtatLivraison().toString());
            statement.setInt(6, livraison.getConducteur().getIdEmploye());
            statement.setInt(7, livraison.getSousCommande().getIdSousCommande());
            statement.setInt(8, livraison.getCamion().getIdCamion());
            statement.setInt(9, livraison.getRemorque().getIdRemorque());
            statement.execute();
            
            for (Adresse adresse : livraison.getItineraire().getAdresses())
                ADRESSE_DAO.update(adresse.getIdAdresse(), adresse);
            
            return livraison;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT itiniraire FROM livraison WHERE idLivraison = ?");
            ResultSet set = statement.executeQuery();
            
            String[] adressesKey = set.getString(9).split(";");
            for (String key : adressesKey) {
                int intKey = Integer.parseInt(key);
                ADRESSE_DAO.remove(intKey);
            }
            
            statement = connection.prepareStatement("DELETE FROM livraison WHERE idLivraison = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException("Impossible de supprimer la livraison");
        }
    }
    
    @Override
    public Livraison get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idLivraison, dateDepart, dateArrivee, sousCommande, conducteur, camion, etatLivraison, remorque, itiniraire FROM livraison WHERE idLivraison = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Livraison livraison = new Livraison();
                livraison.setIdLivraison(set.getInt(1));
                livraison.setDateDepart(set.getDate(2));
                livraison.setDateArrivee(set.getDate(3));
                livraison.setSousCommande(SOUS_COMMANDE_DAO.get(set.getInt(4)));
                livraison.setConducteur(CONDUCTEUR_DAO.get(set.getInt(5)));
                livraison.setCamion(CAMION_DAO.get(set.getInt(6)));
                livraison.setEtatLivraison(EtatLivraison.parse(set.getString(7)));
                livraison.setRemorque(REMORQUE_DAO.get(set.getInt(8)));
                
                String[] adressesKey = set.getString(9).split(";");
                Itineraire itineraire = new Itineraire();
                for (String key : adressesKey) {
                    int intKey = Integer.parseInt(key);
                    itineraire.getAdresses().add(ADRESSE_DAO.get(intKey));
                }
                livraison.setItineraire(itineraire);
                return livraison;
            }
            throw new SQLException("Impossible de récupérer la livraison");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<Livraison> getList() {
        List<Livraison> livraisons = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idLivraison, dateDepart, dateArrivee, sousCommande, conducteur, camion, etatLivraison, remorque, itiniraire FROM livraison");
            ResultSet set = statement.executeQuery();
            
            while (set.next()) {
                Livraison livraison = new Livraison();
                livraison.setIdLivraison(set.getInt(1));
                livraison.setDateDepart(set.getDate(2));
                livraison.setDateArrivee(set.getDate(3));
                livraison.setSousCommande(SOUS_COMMANDE_DAO.get(set.getInt(4)));
                livraison.setConducteur(CONDUCTEUR_DAO.get(set.getInt(5)));
                livraison.setCamion(CAMION_DAO.get(set.getInt(6)));
                livraison.setEtatLivraison(EtatLivraison.parse(set.getString(7)));
                livraison.setRemorque(REMORQUE_DAO.get(set.getInt(8)));
                
                String[] adressesKey = set.getString(9).split(";");
                Itineraire itineraire = new Itineraire();
                for (String key : adressesKey) {
                    int intKey = Integer.parseInt(key);
                    itineraire.getAdresses().add(ADRESSE_DAO.get(intKey));
                }
                livraison.setItineraire(itineraire);
                livraisons.add(livraison);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return livraisons;
    }
    
    // TODO Implémenter
    public List<Livraison> getByClient(int idClient) {
        return new ArrayList<>();
    }
    
    public Livraison getBySousCommande(int idSousCommande) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idLivraison, dateDepart, dateArrivee, sousCommande, conducteur, camion, etatLivraison, remorque, itiniraire FROM livraison WHERE sousCommande = ?");
            statement.setInt(1, idSousCommande);
            ResultSet set = statement.executeQuery();
        
            if (set.next()) {
                Livraison livraison = new Livraison();
                livraison.setIdLivraison(set.getInt(1));
                livraison.setDateDepart(set.getDate(2));
                livraison.setDateArrivee(set.getDate(3));
                livraison.setSousCommande(SOUS_COMMANDE_DAO.get(set.getInt(4)));
                livraison.setConducteur(CONDUCTEUR_DAO.get(set.getInt(5)));
                livraison.setCamion(CAMION_DAO.get(set.getInt(6)));
                livraison.setEtatLivraison(EtatLivraison.parse(set.getString(7)));
                livraison.setRemorque(REMORQUE_DAO.get(set.getInt(8)));
            
                String[] adressesKey = set.getString(9).split(";");
                Itineraire itineraire = new Itineraire();
                for (String key : adressesKey) {
                    int intKey = Integer.parseInt(key);
                    itineraire.getAdresses().add(ADRESSE_DAO.get(intKey));
                }
                livraison.setItineraire(itineraire);
                return livraison;
            }
            throw new SQLException("Impossible de récupérer la livraison");
        }
        catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }

    public List<Livraison> search(String nomClient, java.util.Date dateCommande,
                                  String immatriculationCamion, String immatriculationRemorque, String nomConducteur, String typeMarchandise,
                                  String villeDepart, String villeArrivee, java.util.Date dateDepart, java.util.Date dateArrivee, String etatLivraison) throws SQLException {

        StringBuilder mainSb = new StringBuilder();         // Requête principale
        StringBuilder innerJoinSb = new StringBuilder();    // Ensemble des INNER JOIN
        StringBuilder whereSb = new StringBuilder();        // Ensemble de la clause WHERE
        ArrayList<Object> parameters = new ArrayList<>();   // Paramètres de la procédure SQL

        boolean whereDone = false;
        boolean sousCommandeInnerJoinDone = false;
        boolean commandeInnerJoinDone = false;

        mainSb.append("SELECT idLivraison, dateDepart, dateArrivee, sousCommande, conducteur, camion, etatLivraison, remorque, itiniraire FROM livraison AS liv");

        if (!nomClient.equals("")) {
            if (!sousCommandeInnerJoinDone) {
                innerJoinSb.append(" INNER JOIN sousCommande AS sc ON liv.sousCommande = sc.idSousCommande");
                sousCommandeInnerJoinDone = true;
            }
            if (!commandeInnerJoinDone) {
                innerJoinSb.append(" INNER JOIN commande AS com ON com.sousCommandes LIKE CONCAT(sc.idSousCommande, ';%') OR com.sousCommandes LIKE CONCAT('%;', sc.idSousCommande, ';%')");
                commandeInnerJoinDone = true;
            }
            innerJoinSb.append(" INNER JOIN client AS cli ON com.client = cli.idClient");
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" cli.nom = ? AND");
            parameters.add(nomClient);
        }

        if (dateCommande != null) {
            if (!sousCommandeInnerJoinDone) {
                innerJoinSb.append(" INNER JOIN sousCommande AS sc ON liv.sousCommande = sc.idSousCommande");
                sousCommandeInnerJoinDone = true;
            }
            if (!commandeInnerJoinDone) {
                innerJoinSb.append(" INNER JOIN commande AS com ON com.sousCommandes LIKE CONCAT(sc.idSousCommande, ';%') OR com.sousCommandes LIKE CONCAT('%;', sc.idSousCommande, ';%')");
                commandeInnerJoinDone = true;
            }
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" com.date = ? AND");
            parameters.add(new java.sql.Date(dateCommande.getTime()));
        }

        if (!immatriculationCamion.equals("")) {
            innerJoinSb.append(" INNER JOIN camion AS cam ON liv.camion = cam.idCamion");
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" cam.immatriculation = ? AND");
            parameters.add(immatriculationCamion);
        }

        if (!immatriculationRemorque.equals("")) {
            innerJoinSb.append(" INNER JOIN remorque AS rem ON liv.remorque = rem.idRemorque");
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" rem.immatriculation = ? AND");
            parameters.add(immatriculationRemorque);
        }

        if (!nomConducteur.equals("")) {
            innerJoinSb.append(" INNER JOIN employe AS emp ON liv.conducteur = emp.idEmploye");
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" emp.nom = ? AND");
            parameters.add(nomConducteur);
        }

        if (!villeDepart.equals("")) {
            if (!sousCommandeInnerJoinDone) {
                innerJoinSb.append(" INNER JOIN sousCommande AS sc ON liv.sousCommande = sc.idSousCommande");
                sousCommandeInnerJoinDone = true;
            }
            innerJoinSb.append(" INNER JOIN adresse AS adrDep ON adrDep.idAdresse = sc.depart");
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" adrDep.ville = ? AND");
            parameters.add(villeDepart);
        }

        if (!villeArrivee.equals("")) {
            if (!sousCommandeInnerJoinDone) {
                innerJoinSb.append(" INNER JOIN sousCommande AS sc ON liv.sousCommande = sc.idSousCommande");
                sousCommandeInnerJoinDone = true;
            }
            innerJoinSb.append(" INNER JOIN adresse AS adrArr ON adrArr.idAdresse = sc.arrivee");
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" adrArr.ville = ? AND");
            parameters.add(villeArrivee);
        }

        if (dateDepart != null) {
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" liv.dateDepart = ? AND");
            parameters.add(new java.sql.Date(dateDepart.getTime()));
        }

        if (dateArrivee != null) {
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" liv.dateArrivee = ? AND");
            parameters.add(new java.sql.Date(dateArrivee.getTime()));
        }

        if (!typeMarchandise.equals("Toutes marchandises")) {
            if (!sousCommandeInnerJoinDone) {
                innerJoinSb.append(" INNER JOIN sousCommande AS sc ON liv.sousCommande = sc.idSousCommande");
                sousCommandeInnerJoinDone = true;
            }
            innerJoinSb.append(" INNER JOIN marchandise AS mar ON sc.marchandises LIKE CONCAT(mar.idMarchandise, ';%') OR sc.marchandises LIKE CONCAT('%;', mar.idMarchandise, ';%')");
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" mar.typeMarchandise = ? AND");
            parameters.add(typeMarchandise);
        }

        if (!etatLivraison.equals("Tous états")) {
            if (!whereDone) {
                whereSb.append(" WHERE");
                whereDone = true;
            }
            whereSb.append(" liv.etatLivraison = ? AND");
            parameters.add(etatLivraison);
        }

        try (Connection connection = DBConnexion.getConnexion()) {

            // Création de la requête
            PreparedStatement statement = connection.prepareStatement(
                    mainSb.toString() + innerJoinSb.toString() + ((whereSb.length() > 0) ? whereSb.substring(0, whereSb.length() - 4) : "")
            );

            // Insertion des paramètres
            for (int idx = 0; idx < parameters.size(); idx++) {
                Object parameter = parameters.get(idx);
                if (parameter.getClass() == String.class) {
                    statement.setString(idx + 1, (String) parameter);
                }
                else if (parameter.getClass() == java.sql.Date.class) {
                    statement.setDate(idx + 1, (java.sql.Date)parameter);
                }
            }

            // Exécution de la requête
            ResultSet set = statement.executeQuery();

            // Récupération et création des livraisons
            ArrayList<Livraison> livraisons = new ArrayList<>();
            while (set.next()) {
                Livraison livraison = new Livraison();

                livraison.setIdLivraison(set.getInt(1));
                livraison.setDateDepart(set.getDate(2));
                livraison.setDateArrivee(set.getDate(3));
                livraison.setSousCommande(SOUS_COMMANDE_DAO.get(set.getInt(4)));
                livraison.setConducteur(CONDUCTEUR_DAO.get(set.getInt(5)));
                livraison.setCamion(CAMION_DAO.get(set.getInt(6)));
                livraison.setEtatLivraison(EtatLivraison.parse(set.getString(7)));
                livraison.setRemorque(REMORQUE_DAO.get(set.getInt(8)));

                String[] adressesKey = set.getString(9).split(";");
                Itineraire itineraire = new Itineraire();
                for (String key : adressesKey) {
                    int intKey = Integer.parseInt(key);
                    itineraire.getAdresses().add(ADRESSE_DAO.get(intKey));
                }
                livraison.setItineraire(itineraire);

                livraisons.add(livraison);
            }

            return livraisons;
        }
        catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
}




