package com.dao;

import java.sql.SQLException;
import java.util.List;

public abstract class BaseDAO<T> {
    
    public abstract T create(T object) throws Exception;
    
    public abstract T update(int id, T object) throws Exception;
    
    public abstract boolean remove(int id) throws Exception;
    
    public abstract T get(int id) throws Exception;
    
    public abstract List<T> getList() throws Exception;
}
