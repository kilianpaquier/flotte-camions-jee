package com.dao.client;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.dao.commun.AdresseDAO;
import com.dao.compte.CompteDAO;
import com.model.client.Client;
import com.model.client.Fidelite;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO extends BaseDAO<Client> {
    
    private final AdresseDAO adresseDAO = new AdresseDAO();
    private final CompteDAO compteDAO = new CompteDAO();
    
    @Override
    public Client create(Client client) throws SQLException {
        client.setAdresse(adresseDAO.create(client.getAdresse()));
        client.setCompte(compteDAO.create(client.getCompte()));
        
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO client(nom, prenom, entreprise, telephone, fidelite, compte, adresse) VALUES(?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, client.getNom());
            statement.setString(2, client.getPrenom());
            statement.setString(3, client.getEntreprise());
            statement.setString(4, client.getTelephone());
            statement.setString(5, client.getFidelite().toString());
            statement.setInt(6, client.getCompte().getIdCompte());
            statement.setInt(7, client.getAdresse().getIdAdresse());
            statement.execute();
            
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                client.setIdClient((int) generatedKeys.getLong(1));
            }
            
            return client;
        }
    }
    
    @Override
    public Client update(int id, Client client) throws SQLException {
        client.setIdClient(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE client SET nom = ?, prenom = ?, entreprise = ?, telephone = ?, fidelite = ?, compte = ?, adresse = ? WHERE idClient = ?");
            statement.setString(1, client.getNom());
            statement.setString(2, client.getPrenom());
            statement.setString(3, client.getEntreprise());
            statement.setString(4, client.getTelephone());
            statement.setString(5, client.getFidelite().toString());
            statement.setInt(6, client.getCompte().getIdCompte());
            statement.setInt(7, client.getAdresse().getIdAdresse());
            statement.setInt(8, client.getIdClient());
            statement.execute();
            
            client.setCompte(compteDAO.update(client.getCompte().getIdCompte(), client.getCompte()));
            client.setAdresse(adresseDAO.update(client.getAdresse().getIdAdresse(), client.getAdresse()));
            
            return client;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM client WHERE idClient = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public Client get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idClient, entreprise, nom, prenom, telephone, compte, adresse, fidelite FROM client WHERE idClient = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Client client = new Client();
                client.setIdClient(id);
                client.setEntreprise(set.getString(2));
                client.setNom(set.getString(3));
                client.setPrenom(set.getString(4));
                client.setTelephone(set.getString(5));
                client.setCompte(compteDAO.get(set.getInt(6)));
                client.setAdresse(adresseDAO.get(set.getInt(7)));
                client.setFidelite(Fidelite.parse(set.getString(8)));
                return client;
            }
            throw new SQLException("Impossible de récupérer le client");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<Client> getList() throws SQLException {
        List<Client> clients = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idClient, entreprise, nom, prenom, telephone, compte, adresse, fidelite FROM client");
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Client client = new Client();
                client.setIdClient(set.getInt(1));
                client.setEntreprise(set.getString(2));
                client.setNom(set.getString(3));
                client.setPrenom(set.getString(4));
                client.setTelephone(set.getString(5));
                client.setCompte(compteDAO.get(set.getInt(6)));
                client.setAdresse(adresseDAO.get(set.getInt(7)));
                client.setFidelite(Fidelite.parse(set.getString(8)));
                clients.add(client);
            }
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
        return clients;
    }
    
    public Client getByCompte(int compteID) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idClient, entreprise, nom, prenom, telephone, compte, adresse, fidelite FROM client WHERE compte = ?");
            statement.setInt(1, compteID);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Client client = new Client();
                client.setIdClient(set.getInt(1));
                client.setEntreprise(set.getString(2));
                client.setNom(set.getString(3));
                client.setPrenom(set.getString(4));
                client.setTelephone(set.getString(5));
                client.setCompte(compteDAO.get(compteID));
                client.setAdresse(adresseDAO.get(set.getInt(7)));
                client.setFidelite(Fidelite.parse(set.getString(8)));
                return client;
            }
            throw new SQLException("Impossible de récupérer le client");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
}
