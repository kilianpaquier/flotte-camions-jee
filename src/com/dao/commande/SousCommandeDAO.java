package com.dao.commande;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.dao.commun.AdresseDAO;
import com.model.commande.Frequence;
import com.model.commande.SousCommande;
import com.model.marchandise.Marchandise;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SousCommandeDAO extends BaseDAO<SousCommande> {
    
    private final MarchandiseDAO marchandiseDAO = new MarchandiseDAO();
    private final AdresseDAO adresseDAO = new AdresseDAO();
    
    @Override
    public SousCommande create(SousCommande sousCommande) throws SQLException {
        sousCommande.setLieuDepart(adresseDAO.create(sousCommande.getLieuDepart()));
        sousCommande.setLieuArrivee(adresseDAO.create(sousCommande.getLieuArrivee()));
        
        List<Marchandise> marchandises = new ArrayList<>();
        for (Marchandise marchandise : sousCommande.getMarchandises()) {
            marchandises.add(marchandiseDAO.create(marchandise));
        }
        sousCommande.setMarchandises(marchandises);

        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO souscommande(dateDepartIdeal, depart, arrivee, idSousCommande, tarif, marchandises, frequence) VALUE (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setDate(1, new Date(sousCommande.getDateDepart().getTime()));
            statement.setInt(2, sousCommande.getLieuDepart().getIdAdresse());
            statement.setInt(3, sousCommande.getLieuArrivee().getIdAdresse());
            statement.setInt(4, sousCommande.getIdSousCommande());
            statement.setDouble(5, sousCommande.getTarif());
            statement.setString(6, sousCommande.getMarchandisesAsStringKey());
            statement.setString(7, sousCommande.getFrequence().toString());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                sousCommande.setIdSousCommande((int)generatedKeys.getLong(1));
            }

            return sousCommande;
        }
    }
    
    @Override
    public SousCommande update(int id, SousCommande sousCommande) throws SQLException {
        sousCommande.setIdSousCommande(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE souscommande SET dateDepartIdeal = ?, depart = ?, arrivee = ?, tarif = ?, marchandises = ?, frequence = ? WHERE idSousCommande = ?");
            statement.setDate(1, new Date(sousCommande.getDateDepart().getTime()));
            statement.setInt(2, sousCommande.getLieuDepart().getIdAdresse());
            statement.setInt(3, sousCommande.getLieuArrivee().getIdAdresse());
            statement.setDouble(4, sousCommande.getTarif());
            statement.setString(5, sousCommande.getMarchandisesAsStringKey());
            statement.setString(6, sousCommande.getFrequence().toString());
            statement.setInt(7, sousCommande.getIdSousCommande());
            statement.execute();
            
            for (Marchandise marchandise : sousCommande.getMarchandises())
                marchandiseDAO.update(marchandise.getIdMarchandise(), marchandise);
            
            return sousCommande;
        } catch (SQLException e) {
            throw new SQLException("Impossible de mettre à jour la sous commande");
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT marchandises FROM souscommande WHERE idSousCommande = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            
            String[] marchandisesKey = set.getString(1).split(";");
            for (String stringKey : marchandisesKey) {
                int intKey = Integer.parseInt(stringKey);
                marchandiseDAO.remove(intKey);
            }
            
            statement = connection.prepareStatement("DELETE FROM souscommande WHERE idSousCommande = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException("Impossible de supprimer la sous commande");
        }
    }
    
    @Override
    public SousCommande get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT dateDepartIdeal, depart, arrivee, tarif, frequence, marchandises FROM souscommande WHERE idSousCommande = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                SousCommande sousCommande = new SousCommande();
                sousCommande.setIdSousCommande(id);
                sousCommande.setDateDepart(set.getDate(1));
                sousCommande.setLieuDepart(adresseDAO.get(set.getInt(2)));
                sousCommande.setLieuArrivee(adresseDAO.get(set.getInt(3)));
                sousCommande.setTarif(set.getDouble(4));
                sousCommande.setFrequence(Frequence.parse(set.getString(5)));
                
                String[] marchandisesKey = set.getString(6).split(";");
                for (String stringKey : marchandisesKey) {
                    int intKey = Integer.parseInt(stringKey);
                    sousCommande.addMarchandise(marchandiseDAO.get(intKey));
                }
                return sousCommande;
            }
            throw new SQLException("Imposible de récupérer la sous commande");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<SousCommande> getList() {
        List<SousCommande> sousCommandes = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idSousCommande, dateDepartIdeal, depart, arrivee, tarif, frequence, marchandises FROM souscommande");
            ResultSet set = statement.executeQuery();
            
            while (set.next()) {
                SousCommande sousCommande = new SousCommande();
                sousCommande.setIdSousCommande(set.getInt(1));
                sousCommande.setDateDepart(set.getDate(2));
                sousCommande.setLieuDepart(adresseDAO.get(set.getInt(3)));
                sousCommande.setLieuArrivee(adresseDAO.get(set.getInt(4)));
                sousCommande.setTarif(set.getDouble(5));
                sousCommande.setFrequence(Frequence.parse(set.getString(6)));
                
                String[] marchandisesKey = set.getString(7).split(";");
                for (String stringKey : marchandisesKey) {
                    int intKey = Integer.parseInt(stringKey);
                    sousCommande.addMarchandise(marchandiseDAO.get(intKey));
                }
                sousCommandes.add(sousCommande);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sousCommandes;
    }
    
    public List<SousCommande> getListWhitoutLivraison() {
        List<SousCommande> sousCommandes = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idSousCommande, dateDepartIdeal, depart, arrivee, tarif, frequence, marchandises FROM souscommande LEFT OUTER JOIN livraison l on souscommande.idSousCommande = l.sousCommande");
            ResultSet set = statement.executeQuery();
            
            while (set.next()) {
                SousCommande sousCommande = new SousCommande();
                sousCommande.setIdSousCommande(set.getInt(1));
                sousCommande.setDateDepart(set.getDate(2));
                sousCommande.setLieuDepart(adresseDAO.get(set.getInt(3)));
                sousCommande.setLieuArrivee(adresseDAO.get(set.getInt(4)));
                sousCommande.setTarif(set.getDouble(5));
                sousCommande.setFrequence(Frequence.parse(set.getString(6)));
                
                String[] marchandisesKey = set.getString(7).split(";");
                for (String stringKey : marchandisesKey) {
                    int intKey = Integer.parseInt(stringKey);
                    sousCommande.addMarchandise(marchandiseDAO.get(intKey));
                }
                sousCommandes.add(sousCommande);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sousCommandes;
    }
}
