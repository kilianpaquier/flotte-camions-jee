package com.dao.commande;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.dao.client.ClientDAO;
import com.dao.livraison.LivraisonDAO;
import com.model.commande.Commande;
import com.model.commande.SousCommande;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommandeDAO extends BaseDAO<Commande> {
    
    private final SousCommandeDAO sousCommandeDAO = new SousCommandeDAO();
    private final ClientDAO clientDAO = new ClientDAO();
    private final LivraisonDAO livraisonDAO = new LivraisonDAO();
    
    @Override
    public Commande create(Commande commande) throws SQLException {
        List<SousCommande> sousCommandes = new ArrayList<>();
        for (SousCommande sousCommande : commande.getSousCommandes()) {
            sousCommandes.add(sousCommandeDAO.create(sousCommande));
        }
        commande.setSousCommandes(sousCommandes);
        
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO commande (client, date, sousCommandes) VALUE (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, commande.getClient().getIdClient());
            statement.setDate(2, new Date(commande.getDateCommande().getTime()));
            statement.setString(3, commande.getSousCommandesAsStringKey());
            statement.execute();
            
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                commande.setIdCommande((int) generatedKeys.getLong(1));
            }
            
            return commande;
        }
    }
    
    @Override
    public Commande update(int id, Commande commande) throws SQLException {
        commande.setIdCommande(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE commande SET date = ?, sousCommandes = ? WHERE idCommande = ?");
            statement.setDate(1, new Date(commande.getDateCommande().getTime()));
            statement.setString(2, commande.getSousCommandesAsStringKey());
            statement.setInt(3, commande.getIdCommande());
            statement.execute();
            
            for (SousCommande sousCommande : commande.getSousCommandes())
                sousCommandeDAO.update(sousCommande.getIdSousCommande(), sousCommande);
            
            return commande;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT sousCommandes FROM commande WHERE idCommande = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            
            String[] sousCommandesKey = resultSet.getString(1).split(";");
            for (String key : sousCommandesKey) {
                int intKey = Integer.parseInt(key);
                sousCommandeDAO.remove(intKey);
            }
            
            statement = connection.prepareStatement("DELETE FROM commande WHERE idCommande = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException("Impossible de supprimer la commande");
        }
    }
    
    @Override
    public Commande get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idCommande, client, date , sousCommandes FROM commande where idCommande = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Commande commande = new Commande();
                commande.setIdCommande(set.getInt(1));
                commande.setClient(clientDAO.get(set.getInt(2)));
                commande.setDateCommande(set.getDate(3));
                
                String[] sousCommandesKey = set.getString(4).split(";");
                for (String key : sousCommandesKey) {
                    int intKey = Integer.parseInt(key);
                    commande.addSousCommande(sousCommandeDAO.get(intKey));
                }
                return commande;
            }
            throw new SQLException("Impossible de récupérer la commande");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<Commande> getList() {
        List<Commande> commandes = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idCommande, client, date , sousCommandes FROM commande");
            ResultSet set = statement.executeQuery();
            
            while (set.next()) {
                Commande commande = new Commande();
                commande.setIdCommande(set.getInt(1));
                commande.setClient(clientDAO.get(set.getInt(2)));
                commande.setDateCommande(set.getDate(3));
                
                String[] sousCommandesKey = set.getString(4).split(";");
                for (String key : sousCommandesKey) {
                    int intKey = Integer.parseInt(key);
                    commande.addSousCommande(sousCommandeDAO.get(intKey));
                }
                commandes.add(commande);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commandes;
    }
    
    public List<Commande> getListWhitoutLivraison() {
        List<Commande> commandes = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idCommande, client, date , sousCommandes FROM commande");
            ResultSet set = statement.executeQuery();
            
            while (set.next()) {
                Commande commande = new Commande();
                commande.setIdCommande(set.getInt(1));
                commande.setClient(clientDAO.get(set.getInt(2)));
                commande.setDateCommande(set.getDate(3));
                
                String[] sousCommandesKey = set.getString(4).split(";");
                for (String key : sousCommandesKey) {
                    int intKey = Integer.parseInt(key);
                    try {
                        livraisonDAO.getBySousCommande(intKey);
                    } catch (SQLException ignored) {
                        commande.addSousCommande(sousCommandeDAO.get(intKey));
                    }
                }
                commandes.add(commande);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commandes;
    }
    
    public List<Commande> getByClient(int idClient) {
        List<Commande> commandes = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idCommande, client, date , sousCommandes FROM commande WHERE client = ?");
            statement.setInt(1, idClient);
            ResultSet set = statement.executeQuery();
            
            while (set.next()) {
                Commande commande = new Commande();
                commande.setIdCommande(set.getInt(1));
                commande.setClient(clientDAO.get(set.getInt(2)));
                commande.setDateCommande(set.getDate(3));
                
                String[] sousCommandesKey = set.getString(4).split(";");
                for (String key : sousCommandesKey) {
                    int intKey = Integer.parseInt(key);
                    commande.addSousCommande(sousCommandeDAO.get(intKey));
                }
                commandes.add(commande);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commandes;
    }
}
