package com.dao.commande;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.model.marchandise.Marchandise;
import com.model.marchandise.TypeMarchandise;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MarchandiseDAO extends BaseDAO<Marchandise> {
    
    @Override
    public Marchandise create(Marchandise marchandise) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO marchandise(nom, volume, typeMarchandise) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, marchandise.getNom());
            statement.setDouble(2, marchandise.getVolume());
            statement.setString(3, marchandise.getTypeMarchandise().toString());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                marchandise.setIdMarchandise((int)generatedKeys.getLong(1));
            }

            return marchandise;
        }
    }
    
    @Override
    public Marchandise update(int id, Marchandise marchandise) throws SQLException {
        marchandise.setIdMarchandise(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE marchandise SET nom = ?, volume = ?, typeMarchandise = ? WHERE idMarchandise = ?");
            statement.setString(1, marchandise.getNom());
            statement.setDouble(2, marchandise.getVolume());
            statement.setString(3, marchandise.getTypeMarchandise().toString());
            statement.setInt(4, marchandise.getIdMarchandise());
            statement.execute();
            return marchandise;
        } catch (SQLException e) {
            throw new SQLException("Impossible de mettre à jour la marchandise");
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM marchandise WHERE idMarchandise = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException("Impossible de supprimer la marchandise");
        }
    }
    
    @Override
    public Marchandise get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT nom, volume, typeMarchandise FROM marchandise WHERE idMarchandise = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                Marchandise marchandise = new Marchandise();
                marchandise.setIdMarchandise(id);
                marchandise.setNom(set.getString(1));
                marchandise.setVolume(set.getDouble(2));
                marchandise.setTypeMarchandise(TypeMarchandise.parse(set.getString(3)));
                return marchandise;
            }
            throw new SQLException("Marchandise innexistante");
        } catch (SQLException e) {
            throw new SQLException("Marchandise innexistante");
        }
    }
    
    @Override
    public List<Marchandise> getList() {
        List<Marchandise> marchandises = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idMarchandise, nom, volume, typeMarchandise FROM marchandise");
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                Marchandise marchandise = new Marchandise();
                marchandise.setIdMarchandise(set.getInt(1));
                marchandise.setNom(set.getString(2));
                marchandise.setVolume(set.getDouble(3));
                marchandise.setTypeMarchandise(TypeMarchandise.parse(set.getString(4)));
                marchandises.add(marchandise);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return marchandises;
    }
}
