package com.dao.compte;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.model.compte.Compte;
import com.model.compte.TypeCompte;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CompteDAO extends BaseDAO<Compte> {
    
    @Override
    public Compte create(Compte compte) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO compte(email, motDePasse, typeCompte) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, compte.getEmail());
            statement.setString(2, compte.getMotDePasse());
            statement.setString(3, compte.getTypeCompte().toString());
            statement.execute();
            
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                compte.setIdCompte((int) generatedKeys.getLong(1));
            }
            
            return compte;
        }
    }
    
    @Override
    public Compte update(int id, Compte compte) throws SQLException {
        compte.setIdCompte(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM compte WHERE motDePasse = ? AND idCompte = ?");
            statement.setString(1, compte.getMotDePasse());
            statement.setInt(2, compte.getIdCompte());
            if (!statement.executeQuery().next())
                throw new SQLException("Mot de passe incorrect");
            
            statement = connection.prepareStatement("UPDATE compte SET email = ?, typeCompte = ? WHERE idCompte = ? AND motDePasse = ?");
            statement.setString(1, compte.getEmail());
            statement.setString(4, compte.getMotDePasse());
            statement.setString(2, compte.getTypeCompte().toString());
            statement.setInt(3, compte.getIdCompte());
            statement.execute();
            return compte;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM compte WHERE idCompte = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public Compte get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idCompte, email, motDePasse, typeCompte FROM compte WHERE idCompte = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Compte compte = new Compte();
                compte.setIdCompte(set.getInt(1));
                compte.setEmail(set.getString(2));
                compte.setMotDePasse(set.getString(3));
                compte.setTypeCompte(TypeCompte.parse(set.getString(4)));
                return compte;
            }
            throw new SQLException("Impossible de récupérer le compte");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<Compte> getList() throws SQLException {
        List<Compte> comptes = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idCompte, email, motDePasse, typeCompte FROM compte");
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Compte compte = new Compte();
                compte.setIdCompte(set.getInt(1));
                compte.setEmail(set.getString(2));
                compte.setMotDePasse(set.getString(3));
                compte.setTypeCompte(TypeCompte.parse(set.getString(4)));
                comptes.add(compte);
            }
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
        return comptes;
    }
    
    public Compte getFromEmailAndMotDePasse(String email, String motDePasse) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idCompte, typeCompte FROM compte WHERE email = ? AND motDePasse = ?");
            statement.setString(1, email);
            statement.setString(2, motDePasse);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Compte compte = new Compte();
                compte.setIdCompte(set.getInt(1));
                compte.setEmail(email);
                compte.setMotDePasse(motDePasse);
                compte.setTypeCompte(TypeCompte.parse(set.getString(2)));
                return compte;
            }
            throw new SQLException("Impossible de récupérer le compte");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
}
