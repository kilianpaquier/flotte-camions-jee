package com.dao.camion;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.model.camion.Remorque;
import com.model.marchandise.TypeMarchandise;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RemorqueDAO extends BaseDAO<Remorque> {
    
    @Override
    public Remorque create(Remorque remorque) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO remorque (capacite, prixJournalier, typeMarchandise, immatriculation) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setDouble(1, remorque.getCapacite());
            statement.setDouble(2, remorque.getPrixJournalier());
            statement.setString(3, remorque.getTypeMarchandise().toString());
            statement.setString(4, remorque.getImmatriculation());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                remorque.setIdRemorque((int)generatedKeys.getLong(1));
            }

            return remorque;
        }
    }
    
    @Override
    public Remorque update(int id, Remorque remorque) throws SQLException {
        remorque.setIdRemorque(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE remorque SET capacite = ?, prixJournalier = ?, typeMarchandise = ?, immatriculation = ? WHERE idRemorque = ?");
            statement.setDouble(1, remorque.getCapacite());
            statement.setDouble(2, remorque.getPrixJournalier());
            statement.setString(3, remorque.getTypeMarchandise().toString());
            statement.setString(4, remorque.getImmatriculation());
            statement.setInt(5, remorque.getIdRemorque());
            statement.execute();
            return remorque;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM remorque WHERE idRemorque = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public Remorque get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM remorque WHERE idRemorque = ?");
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                Remorque remorque = new Remorque();
                remorque.setIdRemorque(id);
                remorque.setImmatriculation(result.getString("immatriculation"));
                remorque.setCapacite(result.getDouble("capacite"));
                remorque.setPrixJournalier(result.getInt("prixJournalier"));
                remorque.setTypeMarchandise(TypeMarchandise.parse(result.getString("typeMarchandise")));
                return remorque;
            }
            throw new SQLException("Impossible de récupérer la remorque");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<Remorque> getList() {
        ArrayList<Remorque> listRemorque = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM remorque");
            ResultSet result = statement.executeQuery();
            
            while (result.next()) {
                Remorque remorque = new Remorque();
                
                remorque.setIdRemorque(result.getInt("idRemorque"));
                remorque.setImmatriculation(result.getString("immatriculation"));
                remorque.setPrixJournalier(result.getInt("prixJournalier"));
                remorque.setCapacite(result.getInt("capacite"));
                remorque.setTypeMarchandise(TypeMarchandise.parse(result.getString("typeMarchandise")));
                
                listRemorque.add(remorque);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return listRemorque;
    }
}


