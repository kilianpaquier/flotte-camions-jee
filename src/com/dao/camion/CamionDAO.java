package com.dao.camion;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.model.camion.Camion;
import com.model.camion.EtatCamion;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CamionDAO extends BaseDAO<Camion> {
    
    
    @Override
    public Camion create(Camion camion) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO camion(immatriculation, prixJournalier, capacite, etatCamion) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, camion.getImmatriculation());
            statement.setDouble(2, camion.getPrixJournalier());
            statement.setDouble(3, camion.getCapacite());
            statement.setString(4, camion.getEtatCamion().toString());
            statement.execute();
            
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                camion.setIdCamion((int) generatedKeys.getLong(1));
            }
            
            return camion;
        }
    }
    
    @Override
    public Camion update(int id, Camion camion) throws SQLException {
        camion.setIdCamion(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE camion SET immatriculation = ?, prixJournalier = ?, capacite = ?, etatCamion = ? WHERE idCamion = ?");
            statement.setString(1, camion.getImmatriculation());
            statement.setDouble(2, camion.getPrixJournalier());
            statement.setDouble(3, camion.getCapacite());
            statement.setString(4, camion.getEtatCamion().toString());
            statement.setInt(5, camion.getIdCamion());
            statement.execute();
            return camion;
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM camion WHERE idCamion = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        }
    }
    
    @Override
    public Camion get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM camion WHERE idCamion = ?");
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                Camion camion = new Camion();
                camion.setIdCamion(id);
                camion.setImmatriculation(result.getString("immatriculation"));
                camion.setPrixJournalier(result.getInt("prixJournalier"));
                camion.setCapacite(result.getInt("capacite"));
                camion.setEtatCamion(EtatCamion.parse(result.getString("etatCamion")));
                return camion;
            }
            throw new SQLException("Impossible de récupérer le camion");
        }
    }
    
    @Override
    public List<Camion> getList() {
        ArrayList<Camion> listCamion = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM camion");
            ResultSet result = statement.executeQuery();
            
            while (result.next()) {
                Camion camion = new Camion();
                
                camion.setIdCamion(result.getInt("idCamion"));
                camion.setImmatriculation(result.getString("immatriculation"));
                camion.setPrixJournalier(result.getInt("prixJournalier"));
                camion.setCapacite(result.getInt("capacite"));
                camion.setEtatCamion(EtatCamion.parse(result.getString("etatCamion")));
                
                listCamion.add(camion);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return listCamion;
    }
    
}

