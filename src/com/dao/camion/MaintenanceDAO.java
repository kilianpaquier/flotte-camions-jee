package com.dao.camion;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.dao.employe.TechnicienDAO;
import com.model.camion.EtatMaintenance;
import com.model.camion.Maintenance;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MaintenanceDAO extends BaseDAO<Maintenance> {
    
    @Override
    public Maintenance create(Maintenance maintenance) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO maintenance(duree, description, technicien, etatCamion, camion) VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, maintenance.getDuree());
            statement.setString(2, maintenance.getDescription());
            statement.setInt(3, maintenance.getTechnicien().getIdEmploye());
            statement.setString(4, maintenance.getEtatMaintenance().toString());
            statement.setInt(5, maintenance.getCamion().getIdCamion());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                maintenance.setIdMaintenance((int)generatedKeys.getLong(1));
            }
            
            return maintenance;
        }
    }
    
    @Override
    public Maintenance update(int id, Maintenance maintenance) {
        maintenance.setIdMaintenance(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE maintenance SET duree = ?, description = ?, technicien = ?, etatCamion = ?, camion = ? WHERE idMaintenance = ?");
            statement.setInt(1, maintenance.getDuree());
            statement.setString(2, maintenance.getDescription());
            statement.setInt(3, maintenance.getTechnicien().getIdEmploye());
            statement.setString(4, maintenance.getEtatMaintenance().toString());
            statement.setInt(5, maintenance.getCamion().getIdCamion());
            statement.setInt(6, maintenance.getIdMaintenance());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return maintenance;
    }
    
    @Override
    public boolean remove(int id) {
        boolean success = false;
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM maintenance WHERE idMaintenance = ?");
            statement.setInt(1, id);
            statement.execute();
            success = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }
    
    @Override
    public Maintenance get(int id) {
        Maintenance maintenance = new Maintenance();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT camion, technicien, duree, description, etatCamion FROM maintenance WHERE idMaintenance = ?");
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            maintenance.setIdMaintenance(id);
            
            TechnicienDAO t = new TechnicienDAO();
            CamionDAO c = new CamionDAO();
            
            maintenance.setTechnicien(t.get(result.getInt("technicien")));
            maintenance.setDuree(result.getInt("duree"));
            maintenance.setDescription(result.getString("description"));
            maintenance.setEtatMaintenance(EtatMaintenance.parse(result.getString("etatCamion")));
            maintenance.setCamion(c.get(result.getInt("camion")));
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return maintenance;
    }
    
    @Override
    public List<Maintenance> getList() {
        ArrayList<Maintenance> listMaintenance = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM maintenance");
            
            while (statement.executeQuery().next()) {
                Maintenance maintenance = new Maintenance();
                ResultSet result = statement.executeQuery();
                maintenance.setIdMaintenance(result.getInt("idMaintenance"));
                
                TechnicienDAO t = new TechnicienDAO();
                CamionDAO c = new CamionDAO();
                
                maintenance.setTechnicien(t.get(result.getInt("technicien")));
                maintenance.setDuree(result.getInt("duree"));
                maintenance.setDescription(result.getString("description"));
                maintenance.setEtatMaintenance(EtatMaintenance.parse(result.getString("etatCamion")));
                maintenance.setCamion(c.get(result.getInt("camion")));
                listMaintenance.add(maintenance);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return listMaintenance;
    }
}
