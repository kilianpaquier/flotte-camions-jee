package com.dao.commun;

import com.dao.BaseDAO;
import com.dao.DBConnexion;
import com.model.commun.Adresse;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdresseDAO extends BaseDAO<Adresse> {
    
    @Override
    public Adresse create(Adresse adresse) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO adresse(rue, ville, codePostal, pays) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, adresse.getRue());
            statement.setString(2, adresse.getVille());
            statement.setString(3, adresse.getCodePostal());
            statement.setString(4, adresse.getPays());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                adresse.setIdAdresse((int)generatedKeys.getLong(1));
            }

            return adresse;
        }
    }
    
    @Override
    public Adresse update(int id, Adresse adresse) throws SQLException {
        adresse.setIdAdresse(id);
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE adresse SET rue = ?, ville = ?, codePostal = ?, pays = ? WHERE idAdresse = ?");
            statement.setString(1, adresse.getRue());
            statement.setString(2, adresse.getVille());
            statement.setString(3, adresse.getCodePostal());
            statement.setString(4, adresse.getPays());
            statement.setInt(5, adresse.getIdAdresse());
            statement.execute();
            return adresse;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public boolean remove(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM adresse WHERE idAdresse = ?");
            statement.setInt(1, id);
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public Adresse get(int id) throws SQLException {
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idAdresse, rue, ville, pays, codePostal FROM adresse WHERE idAdresse = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Adresse adresse = new Adresse();
                adresse.setIdAdresse(set.getInt(1));
                adresse.setRue(set.getString(2));
                adresse.setVille(set.getString(3));
                adresse.setPays(set.getString(4));
                adresse.setCodePostal(set.getString(5));
                return adresse;
            }
            throw new SQLException("Impossible de récupérer l'adresse");
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    @Override
    public List<Adresse> getList() throws SQLException {
        List<Adresse> adresses = new ArrayList<>();
        try (Connection connection = DBConnexion.getConnexion()) {
            PreparedStatement statement = connection.prepareStatement("SELECT idAdresse, rue, ville, pays, codePostal FROM adresse");
            ResultSet set = statement.executeQuery();
            
            if (set.next()) {
                Adresse adresse = new Adresse();
                adresse.setIdAdresse(set.getInt(1));
                adresse.setRue(set.getString(2));
                adresse.setVille(set.getString(3));
                adresse.setPays(set.getString(4));
                adresse.setCodePostal(set.getString(5));
                adresses.add(adresse);
            }
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
        return adresses;
    }
}
