-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 28 oct. 2019 à 16:31
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `architecture`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse`
(
    `idAdresse`  int(11)     NOT NULL,
    `ville`      varchar(45) NOT NULL,
    `pays`       varchar(45) NOT NULL,
    `rue`        varchar(45) NOT NULL,
    `codePostal` varchar(45) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `camion`
--

CREATE TABLE `camion`
(
    `idCamion`        int(11)            NOT NULL,
    `immatriculation` varchar(45) UNIQUE NOT NULL,
    `prixJournalier`  double             NOT NULL,
    `capacite`        double             NOT NULL,
    `etatCamion`      enum ('En maintenance','En panne','En fonction','Disponible') DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client`
(
    `idClient`   int(11) NOT NULL,
    `entreprise` varchar(45)                             DEFAULT NULL,
    `nom`        varchar(45)                             DEFAULT NULL,
    `prenom`     varchar(45)                             DEFAULT NULL,
    `telephone`  varchar(45)                             DEFAULT NULL,
    `compte`     int(11)                                 DEFAULT NULL,
    `adresse`    int(11) NOT NULL,
    `fidelite`   enum ('Bronze','Argent','Or','Platine') DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande`
(
    `idCommande`    int(11)     NOT NULL,
    `client`        int(11)     NOT NULL,
    `date`          datetime    NOT NULL,
    `sousCommandes` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte`
(
    `idCompte`   int(11)            NOT NULL,
    `email`      varchar(45) UNIQUE NOT NULL,
    `motDePasse` varchar(45)        NOT NULL,
    `typeCompte` enum ('Client','Opérateur') DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `conducteur`
--

CREATE TABLE `conducteur`
(
    `idEmploye` int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe`
(
    `idEmploye`     int(11)     NOT NULL,
    `nom`           varchar(45) NOT NULL,
    `prenom`        varchar(45) NOT NULL,
    `disponibilite` tinyint(1)  NOT NULL,
    `ville`         varchar(45) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `livraison`
--

CREATE TABLE `livraison`
(
    `idLivraison`   int(11)      NOT NULL,
    `dateDepart`    datetime                                    DEFAULT NULL,
    `dateArrivee`   datetime                                    DEFAULT NULL,
    `sousCommande`  int(11)      NOT NULL,
    `conducteur`    int(11)      NOT NULL,
    `camion`        int(11)      NOT NULL,
    `remorque`      int(11)      NOT NULL,
    `etatLivraison` enum ('En préparation','En cours','Livrée') DEFAULT NULL,
    `itiniraire`    varchar(200) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `maintenance`
--

CREATE TABLE `maintenance`
(
    `idMaintenance` int(11) NOT NULL,
    `camion`        int(11) NOT NULL,
    `technicien`    int(11)                      DEFAULT NULL,
    `duree`         int(11)                      DEFAULT NULL,
    `description`   varchar(45)                  DEFAULT NULL,
    `etat`          enum ('En cours','Terminée') DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `marchandise`
--

CREATE TABLE `marchandise`
(
    `idMarchandise`   int(11)     NOT NULL,
    `nom`             varchar(45) NOT NULL,
    `volume`          double                                                                                              DEFAULT NULL,
    `typeMarchandise` enum ('Frais','Fragile','Congelé','Animal','Viande','Poisson','Essence','Matériel de construction') DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `operateur`
--

CREATE TABLE `operateur`
(
    `idEmploye` int(11)     NOT NULL,
    `compte`    int(11)     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `remorque`
--

CREATE TABLE `remorque`
(
    `idRemorque`      int(11)            NOT NULL,
    `capacite`        double             NOT NULL,
    `prixJournalier`  double             NOT NULL,
    `immatriculation` varchar(50) UNIQUE NOT NULL,
    `typeMarchandise` enum ('Frais','Fragile','Congelé','Animal','Viande','Poisson','Essence','Matériel de construction') DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Structure de la table `souscommande`
--

CREATE TABLE `souscommande`
(
    `idSousCommande`  int(11)      NOT NULL,
    `dateDepartIdeal` datetime     NOT NULL,
    `depart`          int(11)      NOT NULL,
    `arrivee`         int(11)      NOT NULL,
    `tarif`           double                                            DEFAULT NULL,
    `marchandises`    varchar(200) NOT NULL,
    `frequence`       enum ('N/A','Quotidien','Hebdomadaire','Mensuel') DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

-- --------------------------------------------------------

--
-- Structure de la table `technicien`
--

CREATE TABLE `technicien`
(
    `idEmploye` int(11)         NOT NULL,
    `competences`  varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
    ADD PRIMARY KEY (`idAdresse`);

--
-- Index pour la table `camion`
--
ALTER TABLE `camion`
    ADD PRIMARY KEY (`idCamion`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
    ADD PRIMARY KEY (`idClient`),
    ADD KEY `FK_adresse_client` (`adresse`),
    ADD KEY `FK_compte_client` (`compte`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
    ADD PRIMARY KEY (`idCommande`),
    ADD KEY `FK_client_commande` (`client`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
    ADD PRIMARY KEY (`idCompte`);

--
-- Index pour la table `conducteur`
--
ALTER TABLE `conducteur`
    ADD PRIMARY KEY (`idEmploye`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
    ADD PRIMARY KEY (`idEmploye`);

--
-- Index pour la table `livraison`
--
ALTER TABLE `livraison`
    ADD PRIMARY KEY (`idLivraison`),
    ADD KEY `FK_camion_livraison` (`camion`),
    ADD KEY `FK_conducteur_livraison` (`conducteur`),
    ADD KEY `FK_remorque_livraison` (`remorque`),
    ADD KEY `FK_sousCommande_livraison` (`sousCommande`);

--
-- Index pour la table `maintenance`
--
ALTER TABLE `maintenance`
    ADD PRIMARY KEY (`idMaintenance`),
    ADD KEY `FK_maintenance_technicien` (`technicien`),
    ADD KEY `FK_maintenance_camion` (`camion`);

--
-- Index pour la table `marchandise`
--
ALTER TABLE `marchandise`
    ADD PRIMARY KEY (`idMarchandise`);

--
-- Index pour la table `operateur`
--
ALTER TABLE `operateur`
    ADD PRIMARY KEY (`idEmploye`);

--
-- Index pour la table `remorque`
--
ALTER TABLE `remorque`
    ADD PRIMARY KEY (`idRemorque`);

--
-- Index pour la table `souscommande`
--
ALTER TABLE `souscommande`
    ADD PRIMARY KEY (`idSousCommande`);

--
-- Index pour la table `technicien`
--
ALTER TABLE `technicien`
    ADD PRIMARY KEY (`idEmploye`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
    MODIFY `idAdresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `camion`
--
ALTER TABLE `camion`
    MODIFY `idCamion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
    MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
    MODIFY `idCommande` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
    MODIFY `idCompte` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
    MODIFY `idEmploye` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `livraison`
--
ALTER TABLE `livraison`
    MODIFY `idLivraison` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `maintenance`
--
ALTER TABLE `maintenance`
    MODIFY `idMaintenance` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `remorque`
--
ALTER TABLE `remorque`
    MODIFY `idRemorque` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `souscommande`
--
ALTER TABLE `souscommande`
    MODIFY `idSousCommande` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `marchandise`
    MODIFY `idMarchandise` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
    ADD CONSTRAINT `FK_adresse_client` FOREIGN KEY (`adresse`) REFERENCES `adresse` (`idAdresse`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `FK_compte_client` FOREIGN KEY (`compte`) REFERENCES `compte` (`idCompte`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
    ADD CONSTRAINT `FK_client_commande` FOREIGN KEY (`client`) REFERENCES `client` (`idClient`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `conducteur`
--
ALTER TABLE `conducteur`
    ADD CONSTRAINT `FK_idEmploye_conducteur` FOREIGN KEY (`idEmploye`) REFERENCES `employe` (`idEmploye`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `livraison`
--
ALTER TABLE `livraison`
    ADD CONSTRAINT `FK_camion_livraison` FOREIGN KEY (`camion`) REFERENCES `camion` (`idCamion`),
    ADD CONSTRAINT `FK_conducteur_livraison` FOREIGN KEY (`conducteur`) REFERENCES `conducteur` (`idEmploye`),
    ADD CONSTRAINT `FK_remorque_livraison` FOREIGN KEY (`remorque`) REFERENCES `remorque` (`idRemorque`),
    ADD CONSTRAINT `FK_sousCommande_livraison` FOREIGN KEY (`sousCommande`) REFERENCES `souscommande` (`idSousCommande`);

--
-- Contraintes pour la table `maintenance`
--
ALTER TABLE `maintenance`
    ADD CONSTRAINT `FK_maintenance_camion` FOREIGN KEY (`camion`) REFERENCES `camion` (`idCamion`),
    ADD CONSTRAINT `FK_maintenance_technicien` FOREIGN KEY (`technicien`) REFERENCES `technicien` (`idEmploye`);

--
-- Contraintes pour la table `operateur`
--
ALTER TABLE `operateur`
    ADD CONSTRAINT `FK_idEmploye_operateur` FOREIGN KEY (`idEmploye`) REFERENCES `employe` (`idEmploye`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `FK_compte_operateur` FOREIGN KEY (`compte`) REFERENCES `compte` (`idCompte`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraines pour la table 'souscommande"
--
ALTER TABLE `souscommande`
    ADD CONSTRAINT `FK_depart_souscommande` FOREIGN KEY (`depart`) REFERENCES `adresse` (`idAdresse`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `FK_arrivee_souscommande` FOREIGN KEY (`arrivee`) REFERENCES `adresse` (`idAdresse`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `technicien`
--
ALTER TABLE `technicien`
    ADD CONSTRAINT `FK_idEmploye_technicien` FOREIGN KEY (`idEmploye`) REFERENCES `employe` (`idEmploye`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
