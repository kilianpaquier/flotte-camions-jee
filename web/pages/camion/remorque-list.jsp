<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Remorques</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h3 class="card-title text-center">Liste des remorques</h3>
            <input type="button" value="Ajouter une remorque" class="btn btn-primary float-right mb-2"
                   data-toggle="modal"
                   data-target="#modalRemorque"/>
            <struts:actionerror cssClass="errorMessage"/>
            <table class="table overflow-auto" style="max-height: 600px">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Immatriculation</th>
                    <th scope="col">Capacité</th>
                    <th scope="col">Prix journalier</th>
                    <th scope="col">Type de marchandises</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <struts:iterator value="remorques">
                    <struts:form method="GET" action="modifier-remorque" theme="simple">
                        <tr>
                            <struts:hidden name="remorque.idRemorque" value="%{idRemorque}" readonly="true"/>
                            <th scope="row"><struts:property value="immatriculation"/></th>
                            <th><struts:property value="capacite"/></th>
                            <th><struts:property value="prixJournalier"/></th>
                            <th><struts:property value="typeMarchandise"/></th>
                            <th><struts:submit cssClass="btn-sm btn-secondary" value="Supprimer"
                                               action="supprimer-remorque"/></th>
                            <th><struts:submit cssClass="btn-sm btn-primary" value="Modifier"/></th>
                        </tr>
                    </struts:form>
                </struts:iterator>
                </tbody>
            </table>
        </div>
    </div>
    <%@ include file="/pages/template/nav.jsp" %>
    <%@ include file="/pages/template/footer.jsp" %>
</body>

    <!-- Modal -->
    <div class="modal fade" id="modalRemorque" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Information sur la remorque</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <struts:form method="POST" action="add-remorque" theme="simple">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12 col-lg-6">
                                <label for="marchandise">Immatriculation</label>
                                <struts:textfield id="marchandise" name="remorque.immatriculation" cssClass="form-control"
                                                  placeholder="Immatriculation de la remorque" required="true"/>
                                <struts:fielderror fieldName="remorque.immatriculation" cssClass="errorMessage mb-n2"/>
                            </div>
                            <div class="form-group col-md-12 col-lg-6">
                                <label for="volume">Capacité de transport</label>
                                <struts:textfield type="number" name="remorque.capacite" min="1" value="1" id="volume"
                                                  cssClass="form-control" required="true"/>
                                <struts:fielderror fieldName="marchandise.volume" cssClass="errorMessage mb-n2"/>
                            </div>
                            <div class="form-group col-md-12 col-lg-6">
                                <label for="typeMarchandise">Type de marchandise</label>
                                <struts:select id="typeMarchandise" name="typeMarchandise" cssClass="custom-select"
                                               list="typeMarchandises" required="true"/>
                                <struts:fielderror fieldName="typeMarchandise" cssClass="errorMessage mb-n2"/>
                            </div>
                            <div class="form-group col-md-12 col-lg-6">
                                <label for="priJ">Prix journaliser</label>
                                <struts:textfield type="number" name="remorque.prixJournalier" min="1" value="1" id="priJ"
                                                  cssClass="form-control" required="true"/>
                                <struts:fielderror fieldName="marchandise.volume" cssClass="errorMessage mb-n2"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                        <struts:submit cssClass="btn btn-primary" value="Ajouter la remorque"/>
                    </div>
                </struts:form>
            </div>
        </div>
    </div>
</html>
