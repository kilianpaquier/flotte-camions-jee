<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Modification de remorque</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-50">
        <div class="card-body">
            <h3 class="card-title">Modification de remorque</h3>
            <struts:form method="POST" action="valider-modification-remorque" theme="simple">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-6">
                        <struts:hidden name="remorque.idRemorque" readonly="true"/>
                        <label for="marchandise">Immatriculation</label>
                        <struts:textfield id="marchandise" name="remorque.immatriculation" cssClass="form-control"
                                          placeholder="Immatriculation de la remorque" required="true"/>
                        <struts:fielderror fieldName="remorque.immatriculation" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="volume">Capacité de transport</label>
                        <struts:textfield type="number" name="remorque.capacite" min="1" id="volume"
                                          cssClass="form-control" required="true"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="typeMarchandise">Type de marchandise</label>
                        <struts:select id="typeMarchandise" name="typeMarchandise" cssClass="custom-select"
                                       list="typeMarchandises" required="true"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="priJ">Prix journaliser</label>
                        <struts:textfield type="number" name="remorque.prixJournalier" min="1" id="priJ"
                                          cssClass="form-control" required="true"/>
                    </div>
                </div>
                <struts:actionmessage cssClass="infoMessage"/>
                <struts:actionerror cssClass="errorMessage"/>
                <struts:submit cssClass="btn btn-primary" value="Modifier la remorque"/>
            </struts:form>
        </div>
    </div>
</div>
<%@ include file="/pages/template/nav.jsp" %>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
