<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Camion</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h3 class="card-title text-center">Liste des camions</h3>
            <input type="button" value="Ajouter un camion" class="btn btn-primary float-right mb-2"
                   data-toggle="modal"
                   data-target="#modalCamion"/>
            <struts:actionmessage/>
            <struts:actionerror cssClass="errorMessage"/>
            <table class="table overflow-auto" style="max-height: 600px">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Immatriculation</th>
                    <th scope="col">Capacité</th>
                    <th scope="col">Prix journalier</th>
                    <th scope="col">État</th>
                    <th scope="col">#</th>
                    <th scope="col">#</th>
                </tr>
                </thead>
                <tbody>
                <struts:iterator value="camions">
                    <tr>
                        <struts:form method="GET" action="readCamion" theme="simple">
                            <struts:hidden name="camion.idCamion" value="%{idCamion}" readonly="true"/>
                            <td><struts:property value="immatriculation"/></td>
                            <td><struts:property value="capacite"/></td>
                            <td><struts:property value="prixJournalier"/></td>
                            <td><struts:property value="etatCamion"/></td>
                            <td><struts:submit cssClass="btn-sm btn-primary" value="Modifier"/></td>
                            <td><struts:submit cssClass="btn-sm btn-secondary" value="Supprimer"
                                               action="deleteCamion"/></td>
                        </struts:form>
                    </tr>
                </struts:iterator>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%@ include file="/pages/template/nav.jsp" %>
<%@ include file="/pages/template/footer.jsp" %>

<!-- Modal -->
<div class="modal fade" id="modalCamion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Information sur la remorque</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <struts:form method="POST" action="createCamion" theme="simple" cssClass="card-body">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="immatriculation">Immatriculation</label>
                            <struts:textfield id="immatriculation" name="camion.immatriculation" cssClass="form-control"
                                              placeholder="Saisissez l'immatriculation" required="true"/>
                            <struts:fielderror fieldName="camion.immatriculation" cssClass="errorMessage"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="prixJournalier">Prix Journalier</label>
                            <struts:textfield id="prixJournalier" name="camion.prixJournalier" cssClass="form-control"
                                              type="number" min="0" step="0.01" value="1"
                                              placeholder="Saisissez le prix journalier" required="true"/>
                            <struts:fielderror fieldName="camion.prixJournalier" cssClass="errorMessage"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="capacite">Capacité</label>
                            <struts:textfield id="capacite" name="camion.capacite" cssClass="form-control"
                                              type="number" min="0" step="0.01" value="1"
                                              placeholder="Saisissez la capacité du camion" required="true"/>
                            <struts:fielderror fieldName="camion.capacite" cssClass="errorMessage"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="etatCamion">État *</label>
                            <struts:select id="etatCamion" name="etatCamion" cssClass="custom-select"
                                           list="etatsCamion"
                                           required="true"/>
                            <struts:fielderror fieldName="etatCamion" cssClass="errorMessage"/>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                        <struts:submit cssClass="btn btn-primary" value="Ajouter le camion"/>
                    </div>
                </div>
            </struts:form>
        </div>
    </div>
</div>
</body>

</html>
