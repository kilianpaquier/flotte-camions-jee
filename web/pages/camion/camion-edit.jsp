<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Modififcation camion</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-75">
        <struts:form method="POST" action="updateCamion" theme="simple" cssClass="card-body">
            <h5 class="card-subtitle mt-2">Informations du camion</h5>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <struts:hidden name="camion.idCamion"/>
                    <label for="immatriculation">Immatriculation *</label>
                    <struts:textfield id="immatriculation" name="camion.immatriculation" cssClass="form-control"
                                      placeholder="Saisissez l'immatriculation" value="%{camion.immatriculation}"
                                      required="true"/>
                    <struts:fielderror fieldName="camion.immatriculation" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="prix">Prix Journalier *</label>
                    <struts:textfield id="prix" name="camion.prixJournalier" cssClass="form-control"
                                      type="number" min="0" step="0.01" placeholder="Saisissez le prix journalier"
                                      value="%{camion.prixJournalier}" required="true"/>
                    <struts:fielderror fieldName="camion.prixJournalier" cssClass="errorMessage"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="capacite">Capacité *</label>
                    <struts:textfield id="capacite" name="camion.capacite" cssClass="form-control"
                                      type="number" min="0" step="0.01" placeholder="Saisissez la capacité du camion"
                                      value="%{camion.capacite}" required="true"/>
                    <struts:fielderror fieldName="camion.capacite" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="etatCamion">État *</label>
                    <struts:select id="etatCamion" name="etatCamion" cssClass="custom-select"
                                   list="etatsCamion"
                                   value="%{camion.etatCamion}"
                                   required="true"/>
                    <struts:fielderror fieldName="camion.etatCamion" cssClass="errorMessage"/>
                </div>
            </div>
            <struts:actionmessage cssClass="infoMessage"/>
            <struts:actionerror cssClass="errorMessage"/>
            <struts:submit cssClass="btn btn-primary" value="Modifier le camion"/>
        </struts:form>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
