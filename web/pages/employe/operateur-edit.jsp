<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Modification d'un opérateur</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-50">
        <div class="card-body">
            <h3 class="card-title">Modification d'un opérateur</h3>
            <struts:form method="POST" action="valider-modifier-operateur" theme="simple">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="marchandise">Nom de l'opérateur</label>
                        <struts:hidden name="operateur.idEmploye" readonly="true"/>
                        <struts:hidden name="operateur.compte.idCompte" readonly="true"/>
                        <struts:textfield id="marchandise" name="operateur.nom" cssClass="form-control"
                                          placeholder="Nom de l'opérateur" required="true"/>
                        <struts:fielderror fieldName="operateur.nom" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="volume">Prénom de l'opérateur</label>
                        <struts:textfield name="operateur.prenom" placeholder="Prénom de l'opérateur" id="volume"
                                          cssClass="form-control" required="true"/>
                        <struts:fielderror fieldName="operateur.prenom" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="typeMarchandise">Ville</label>
                        <struts:textfield name="operateur.ville" placeholder="Ville de l'opérateur" id="typeMarchandise"
                                          cssClass="form-control" required="true"/>
                        <struts:fielderror fieldName="operateur.ville" cssClass="errorMessage mb-n2"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="priJ">Email du compte</label>
                        <struts:textfield name="operateur.compte.email" placeholder="Email de l'opérateur" id="priJ"
                                          cssClass="form-control" required="true"/>
                        <struts:fielderror fieldName="operateur.compte.email" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="mdp">Mot de passe du compte</label>
                        <struts:password name="operateur.compte.motDePasse" placeholder="Mot de passe de l'opérateur"
                                         id="mdp"
                                         cssClass="form-control" required="true"/>
                        <struts:fielderror fieldName="operateur.compte.motDePasse" cssClass="errorMessage mb-n2"/>
                    </div>
                </div>
                <struts:actionmessage cssClass="infoMessage"/>
                <struts:actionerror cssClass="errorMessage"/>
                <struts:submit cssClass="btn btn-primary" value="Modifier l'opérateur"/>
            </struts:form>
        </div>
    </div>
</div>
<%@ include file="/pages/template/nav.jsp" %>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
