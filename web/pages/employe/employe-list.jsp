<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Liste des employés</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h3 class="card-title text-center">Liste des employés</h3>
            <input type="button" value="Ajouter un conducteur" class="ml-1 btn btn-primary float-right mb-2"
                   data-toggle="modal"
                   data-target="#modalConducteur"/>
            <input type="button" value="Ajouter un opérateur" class="ml-1 btn btn-primary float-right mb-2"
                   data-toggle="modal"
                   data-target="#modalOperateur"/>
            <struts:form method="POST" action="add-technicien" cssClass="float-right">
                <struts:submit value="Ajouter un technicien" cssClass="btn btn-primary float-right mb-2"/>
            </struts:form>
            <struts:actionerror cssClass="errorMessage"/>
            <table class="table overflow-auto" style="max-height: 600px">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Identifiant</th>
                    <th scope="col">Nom et Prénom</th>
                    <th scope="col">Disponibilité</th>
                    <th scope="col">Ville</th>
                    <th scope="col">Type employé</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <struts:iterator value="conducteurs">
                    <struts:form method="GET" action="modifier-conducteur" theme="simple">
                        <tr>
                            <struts:hidden name="conducteur.idEmploye" value="%{idEmploye}" readonly="true"/>
                            <th scope="row"><struts:property value="idEmploye"/></th>
                            <th><struts:property value="nom"/> <struts:property value="prenom"/></th>
                            <th><struts:property value="disponible"/></th>
                            <th><struts:property value="ville"/></th>
                            <th>Conducteur</th>
                            <th><struts:submit cssClass="btn-sm btn-secondary" value="Supprimer"
                                               action="delete-conducteur"/></th>
                            <th><struts:submit cssClass="btn-sm btn-primary" value="Modifier"/></th>
                        </tr>
                    </struts:form>
                </struts:iterator>
                <struts:iterator value="operateurs">
                    <struts:form method="GET" action="modifier-operateur" theme="simple">
                        <tr>
                            <struts:hidden name="operateur.idEmploye" value="%{idEmploye}" readonly="true"/>
                            <th scope="row"><struts:property value="idEmploye"/></th>
                            <th><struts:property value="nom"/> <struts:property value="prenom"/></th>
                            <th><struts:property value="disponible"/></th>
                            <th><struts:property value="ville"/></th>
                            <th>Opérateur</th>
                            <th><struts:submit cssClass="btn-sm btn-secondary" value="Supprimer"
                                               action="delete-operateur"/></th>
                            <th><struts:submit cssClass="btn-sm btn-primary" value="Modifier"/></th>
                        </tr>
                    </struts:form>
                </struts:iterator>
                <struts:iterator value="techniciens">
                    <struts:form method="GET" action="modifier-technicien" theme="simple">
                        <tr>
                            <struts:hidden name="technicien.idEmploye" value="%{idEmploye}" readonly="true"/>
                            <th scope="row"><struts:property value="idEmploye"/></th>
                            <th><struts:property value="nom"/> <struts:property value="prenom"/></th>
                            <th><struts:property value="disponible"/></th>
                            <th><struts:property value="ville"/></th>
                            <th>Technicien</th>
                            <th><struts:submit cssClass="btn-sm btn-secondary" value="Supprimer"
                                               action="delete-technicien"/></th>
                            <th><struts:submit cssClass="btn-sm btn-primary" value="Modifier"/></th>
                        </tr>
                    </struts:form>
                </struts:iterator>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%@ include file="/pages/template/nav.jsp" %>
<%@ include file="/pages/template/footer.jsp" %>
</body>

<!-- Modal -->
<div class="modal fade" id="modalOperateur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Information sur l'opérateur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <struts:form method="POST" action="add-operateur" theme="simple">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="marchandise">Nom de l'opérateur</label>
                            <struts:textfield id="marchandise" name="operateur.nom" cssClass="form-control"
                                              placeholder="Nom de l'opérateur" required="true"/>
                            <struts:fielderror fieldName="operateur.nom" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="volume">Prénom de l'opérateur</label>
                            <struts:textfield name="operateur.prenom" placeholder="Prénom de l'opérateur" id="volume"
                                              cssClass="form-control" required="true"/>
                            <struts:fielderror fieldName="operateur.prenom" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="typeMarchandise">Ville</label>
                            <struts:textfield name="operateur.ville" placeholder="Ville de l'opérateur" id="typeMarchandise"
                                              cssClass="form-control" required="true"/>
                            <struts:fielderror fieldName="operateur.ville" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="priJ">Email du compte</label>
                            <struts:textfield name="operateur.compte.email" placeholder="Email de l'opérateur" id="priJ"
                                              cssClass="form-control" required="true"/>
                            <struts:fielderror fieldName="operateur.compte.email" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="mdp">Mot de passe du compte</label>
                            <struts:password name="operateur.compte.motDePasse" placeholder="Mot de passe de l'opérateur" id="mdp"
                                              cssClass="form-control" required="true"/>
                            <struts:fielderror fieldName="operateur.compte.motDePasse" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <struts:submit cssClass="btn btn-primary" value="Ajouter l'opérateur"/>
                </div>
            </struts:form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalConducteur" tabindex="-1" role="dialog" aria-labelledby="modalConducteurLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConducteurLabel">Information sur le conducteur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <struts:form method="POST" action="add-conducteur" theme="simple">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12 col-lg-6">
                            <label>Nom du conducteur</label>
                            <struts:textfield name="conducteur.nom" cssClass="form-control"
                                              placeholder="Nom du conducteur" required="true"/>
                            <struts:fielderror fieldName="conducteur.nom" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="volume">Prénom du conducteur</label>
                            <struts:textfield name="conducteur.prenom" placeholder="Prénom du conducteur"
                                              cssClass="form-control" required="true"/>
                            <struts:fielderror fieldName="conducteur.prenom" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="typeMarchandise">Ville</label>
                            <struts:textfield name="conducteur.ville" placeholder="Ville du conducteur"
                                              cssClass="form-control" required="true"/>
                            <struts:fielderror fieldName="conducteur.ville" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <struts:submit cssClass="btn btn-primary" value="Ajouter le conducteur"/>
                </div>
            </struts:form>
        </div>
    </div>
</div>
</html>
