<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Modification d'un technicien</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-75">
        <div class="card-body">
            <h3 class="card-title text-center mb-2">Modification d'un technicien</h3>
            <struts:form method="POST" action="valider-modifier-technicien" theme="simple">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <label class="card-subtitle">Liste des compétences</label>
                        <ul class="list-group mt-2 overflow-auto" style="max-height: 300px">
                            <struts:if test="%{technicien.competences.size() == 0}">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Pas de compétences enregistrées.
                                </li>
                            </struts:if>
                            <struts:iterator value="technicien.competences">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <struts:property value="valeur"/>
                                </li>
                            </struts:iterator>
                        </ul>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <div class="row">
                            <div class="form-group col-md-12 col-lg-6">
                                <label for="marchandise">Nom du technicien</label>
                                <struts:hidden name="technicien.idEmploye" readonly="true"/>
                                <struts:textfield id="marchandise" name="technicien.nom" cssClass="form-control"
                                                  placeholder="Nom du technicien" required="true"/>
                                <struts:fielderror fieldName="technicien.nom" cssClass="errorMessage mb-n2"/>
                            </div>
                            <div class="form-group col-md-12 col-lg-6">
                                <label for="volume">Prénom du technicien</label>
                                <struts:textfield name="technicien.prenom" placeholder="Prénom du technicien"
                                                  id="volume"
                                                  cssClass="form-control" required="true"/>
                                <struts:fielderror fieldName="technicien.prenom" cssClass="errorMessage mb-n2"/>
                            </div>
                            <div class="form-group col-md-12 col-lg-6">
                                <label>Ville</label>
                                <struts:textfield name="technicien.ville" placeholder="Ville du technicien"
                                                  cssClass="form-control" required="true"/>
                                <struts:fielderror fieldName="technicien.ville" cssClass="errorMessage mb-n2"/>
                            </div>
                        </div>
                        <struts:actionmessage cssClass="infoMessage"/>
                        <struts:actionerror cssClass="errorMessage"/>
                        <struts:submit cssClass="btn btn-primary" value="Modifier le technicien"/>
                    </div>
                </div>
            </struts:form>
        </div>
    </div>
</div>
<%@ include file="/pages/template/nav.jsp" %>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
