<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Modification d'un conducteur</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-50">
        <div class="card-body">
            <h3 class="card-title">Modification d'un conducteur</h3>
            <struts:form method="POST" action="valider-modifier-conducteur" theme="simple">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="marchandise">Nom du conducteur</label>
                        <struts:hidden name="conducteur.idEmploye" readonly="true"/>
                        <struts:textfield id="marchandise" name="conducteur.nom" cssClass="form-control"
                                          placeholder="Nom du conducteur" required="true"/>
                        <struts:fielderror fieldName="conducteur.nom" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label for="volume">Prénom du conducteur</label>
                        <struts:textfield name="conducteur.prenom" placeholder="Prénom du conducteur" id="volume"
                                          cssClass="form-control" required="true"/>
                        <struts:fielderror fieldName="conducteur.prenom" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Ville</label>
                        <struts:textfield name="conducteur.ville" placeholder="Ville du conducteur"
                                          cssClass="form-control" required="true"/>
                        <struts:fielderror fieldName="conducteur.ville" cssClass="errorMessage mb-n2"/>
                    </div>
                </div>
                <struts:actionmessage cssClass="infoMessage"/>
                <struts:actionerror cssClass="errorMessage"/>
                <struts:submit cssClass="btn btn-primary" value="Modifier le conducteur"/>
            </struts:form>
        </div>
    </div>
</div>
<%@ include file="/pages/template/nav.jsp" %>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
