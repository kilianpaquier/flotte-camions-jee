<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Validation de commande</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <struts:form action="submit-validorder" method="POST" theme="simple">
                <h3 class="card-title text-center mb-2">Création de la livraison</h3>
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h5 class="card-subtitle">Informations de la commande</h5>
                        <div class="form-group">
                            <label for="commande">Identifiant sous commande</label>
                            <struts:textfield id="commande" name="livraison.sousCommande.idSousCommande"
                                              cssClass="form-control"
                                              placeholder="Identifiant sous commande" readonly="true"/>
                            <struts:fielderror fieldName="livraison.sousCommande.id"
                                               cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group">
                            <label for="depart">Lieu de départ</label>
                            <struts:textfield id="depart" name="livraison.sousCommande.lieuDepart.ville"
                                              cssClass="form-control"
                                              placeholder="Lieu de départ" readonly="true"/>
                            <struts:fielderror fieldName="livraison.sousCommande.lieuDepart"
                                               cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group">
                            <label for="arrivee">Lieu d'arrivée</label>
                            <struts:textfield id="arrivee" name="livraison.sousCommande.lieuArrivee.ville"
                                              cssClass="form-control"
                                              placeholder="Lieu d'arrivée" readonly="true"/>
                            <struts:fielderror fieldName="livraison.sousCommande.lieuArrivee"
                                               cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group">
                            <label>Volume total de la sous commande</label>
                            <struts:textfield cssClass="form-control"
                                              value="%{livraison.sousCommande.countMarchandises()} m³"
                                              placeholder="Lieu d'arrivée" readonly="true"/>
                            <struts:fielderror fieldName="livraison.sousCommande.countMarchandise"
                                               cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <h5 class="card-subtitle">Informations de la livraison</h5>
                        <struts:hidden name="livraison.idLivraison"/>
                        <div class="form-group">
                            <label for="camion">Camion de transport *</label>
                            <struts:select id="camion" name="camion" cssClass="custom-select" list="camions"
                                           required="true"/>
                            <struts:fielderror fieldName="livraison.camion" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group">
                            <label for="conducteur">Conducteur de transport *</label>
                            <struts:select id="conducteur" name="conducteur" cssClass="custom-select"
                                           list="conducteurs" required="true"/>
                            <struts:fielderror fieldName="livraison.conducteur" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group">
                            <label for="remorque">Remorque de transport *</label>
                            <struts:select id="remorque" name="remorque" cssClass="custom-select"
                                           list="remorques" required="true"/>
                            <struts:fielderror fieldName="livraison.remorque" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="dated">Date de départ *</label>
                                <struts:textfield id="dated" type="date" name="livraison.dateDepart"
                                                  cssClass="form-control"
                                                  placeholder="Saisissez la date de départ"
                                                  value="%{livraison.dateDepart}" required="true"/>
                                <struts:fielderror fieldName="livraison.dateDepart" cssClass="errorMessage mb-n2"/>
                            </div>
                            <div class="form-group col-6">
                                <label>Date d'arrivée : </label>
                                <struts:hidden name="livraison.dateArrivee"/>
                                <struts:date name="livraison.dateArrivee" format="dd/MM/yyyy"/>
                            </div>
                        </div>
                        <label class="mt-1">Itinéraire</label>
                        <struts:submit value="Calculer l'itinéraire" action="calcul-itineraire"
                                       cssClass="btn-sm btn-secondary float-right"/>
                        <div class="list-group mb-3 overflow-auto" style="max-height: 200px">
                            <struts:if test="%{livraison.itineraire == null}">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Pas d'itinéraire calculé.
                                </li>
                            </struts:if>
                            <struts:iterator value="livraison.itineraire.adresses">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <struts:property value="ville"/> - <struts:property value="pays"/>
                                    <span class="badge badge-primary badge-pill"><struts:property
                                            value="codePostal"/></span>
                                </li>
                            </struts:iterator>
                            <label class="infoMessage">L'itinéraire sera aussi calculer dans la validation - Ici vous aurez un visuel de celui-ci</label>
                        </div>
                        <div>
                            <struts:actionmessage cssClass="infoMessage mb-n1 mt-1"/>
                            <struts:actionerror cssClass="errorMessage mb-n1 mt-1"/>
                            <struts:submit cssClass="btn btn-primary float-right" value="Créer la livraison"/>
                        </div>
                    </div>
                </div>
            </struts:form>
        </div>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
