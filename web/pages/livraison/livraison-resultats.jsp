<%@ taglib prefix="struts" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Kilian
  Date: 25/10/2019
  Time: 17:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Résultats livraisons</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h3 class="card-title text-center">Liste des livraisons</h3>
            <struts:if test="livraisons.size == 0">
                Aucun résultat trouvé.
            </struts:if>
            <struts:iterator value="livraisons">
                <h4 class="card-subtitle mb-2">Numéro de livraison : <struts:property value="idLivraison"/></h4>
                <h6 class="card-subtitle">Informations de la commande</h6>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <label>Marchandises</label>
                        <ul class="list-group mt-2 overflow-auto" style="max-height: 300px">
                            <struts:iterator value="sousCommande.marchandises">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <struts:property value="nom"/> - <struts:property value="typeMarchandise"/>
                                    <div class="float-right">
                                <span class="badge badge-primary badge-pill">
                                    <struts:property value="volume"/> m³
                                </span>
                                    </div>
                                </li>
                            </struts:iterator>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                        <div class="row">
                            <label class="font-weight-bold">Lieu de départ</label> : <struts:property value="sousCommande.lieuDepart"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Lieu d'arrivée</label> : <struts:property value="sousCommande.lieuArrivee"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Date de départ</label> : <struts:property value="dateDepart"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Date d'arrivée</label> : <struts:property value="dateArrivee"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Fréquence de livraison</label> : <struts:property
                                    value="sousCommande.frequence.toString()"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Tarif</label> : <struts:property value="sousCommande.tarif"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Immatriculation camion</label> : <struts:property value="camion.immatriculation"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Immatriculation remorque</label> : <struts:property value="remorque.immatriculation"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">Conducteur</label> : <struts:property value="conducteur.nom"/> <struts:property
                                    value="conducteur.prenom"/>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">État de la livraison</label> : <struts:property value="etatLivraison.toString()"/>
                        </div>
                    </div>
                </div>
            </struts:iterator>
        </div>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
