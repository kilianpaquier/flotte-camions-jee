<%@ taglib prefix="struts" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Kilian
  Date: 25/10/2019
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Recherche avancée des livraisons</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
    <%@ include file="/pages/template/nav.jsp" %>
    <div class="container mt-5 py-5">
        <div class="card mx-auto ">
            <div class="card-body">
                <h3 class="card-title">Recherche avancée de livraisons</h3>
                <p class="infoMessage">Laissez les champs vide pour ne pas en tenir compte dans la recherche</p>
                <struts:actionerror cssClass="errorMessage"/>
                <struts:form theme="simple" action="result-search-delivery" method="POST">
                    <h5 class="card-subtitle mt-2">Informations commande</h5>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="nomClient">Client</label>
                            <struts:textfield id="nomClient" name="nomClient" cssClass="form-control"
                                              placeholder="Nom du client"/>
                            <struts:fielderror fieldName="nomClient" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="dateCommande">Date de commande</label>
                            <struts:textfield id="dateCommande" name="dateCommande" cssClass="form-control"
                                              placeholder="Date de la commande" type="date"/>
                            <struts:fielderror fieldName="dateCommande" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <h5 class="card-subtitle mt-2">Informations livraison</h5>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="conducteur">Conducteur</label>
                            <struts:textfield id="conducteur" name="nomConducteur" cssClass="form-control"
                                              placeholder="Nom de famille du conducteur"/>
                            <struts:fielderror fieldName="conducteur" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="immatriculationCamion">Camion</label>
                            <struts:textfield id="immatriculationCamion" name="immatriculationCamion" cssClass="form-control"
                                              placeholder="Immatriculation du camion"/>
                            <struts:fielderror fieldName="immatriculationCamion" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="immatriculationRemorque">Remorque</label>
                            <struts:textfield id="immatriculationRemorque" name="immatriculationRemorque" cssClass="form-control"
                                              placeholder="Immatriculation de la remorque"/>
                            <struts:fielderror fieldName="immatriculationRemorque" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="villed">Ville de départ</label>
                            <struts:textfield id="villed" name="villeDepart" cssClass="form-control"
                                              placeholder="Ville de départ"/>
                            <struts:fielderror fieldName="villeDepart" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="villea">Ville d'arrivée</label>
                            <struts:textfield id="villea" name="villeArrivee" cssClass="form-control"
                                              placeholder="Ville d'arrivée"/>
                            <struts:fielderror fieldName="villeArrivee" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="dateDepart">Date de départ</label>
                            <struts:textfield id="dateDepart" name="dateDepart" cssClass="form-control"
                                              placeholder="Date de départ" type="date"/>
                            <struts:fielderror fieldName="dateDepart" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4 form-group">
                            <label for="dateArrivee">Date d'arrivée</label>
                            <struts:textfield id="dateArrivee" name="dateArrivee" cssClass="form-control"
                                              placeholder="Date d'arrivée" type="date"/>
                            <struts:fielderror fieldName="dateArrivee" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12 col-md-6 col-lg-4">
                            <label for="typeMarchandise">Type de marchandise</label>
                            <struts:select id="typeMarchandise" name="typeMarchandise" cssClass="custom-select"
                                           list="typeMarchandises"/>
                            <struts:fielderror fieldName="typeMarchandise" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-sm-12 col-md-6 col-lg-4">
                            <label for="etatLivraison">État de la livraison</label>
                            <struts:select id="etatLivraison" name="etatLivraison" cssClass="custom-select"
                                           list="etatLivraisons"/>
                            <struts:fielderror fieldName="etatLivraison" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <struts:submit cssClass="btn btn-primary float-right" value="Rechercher les livraisons"/>
                </struts:form>
            </div>
        </div>
    </div>
    <%@ include file="/pages/template/footer.jsp" %>
</body>
</html>