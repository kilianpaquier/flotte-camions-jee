<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Livraisons en cours</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h3 class="card-title text-center">Liste des livraisons</h3>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-lg-4 mb-2">
                    <struts:form method="POST" theme="simple" action="search-delivery">
                        <struts:submit cssClass="btn btn-secondary" value="Recherche avancée"/>
                    </struts:form>
                </div>
            </div>
            <div class="row">
                <struts:form method="POST" theme="simple" cssClass="col-md-6 col-sm-12 col-lg-4"
                             action="simple-search-delivery">
                    <div class="form-group">
                        <label for="numcommande">Recherche par numéro de commande</label>
                        <struts:textfield id="numcommande" type="number" name="idCommande" cssClass="form-control"
                                          placeholder="Numéro de commande"/>
                        <struts:fielderror fieldName="idCommande" cssClass="errorMessage mb-n2"/>
                    </div>
                </struts:form>
            </div>
            <table class="table overflow-auto" style="max-height: 600px">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Conducteur</th>
                    <th scope="col">Camion</th>
                    <th scope="col">Remorque</th>
                    <th scope="col">Date de départ</th>
                    <th scope="col">Date d'arrivée</th>
                    <th scope="col">Lieu de départ</th>
                    <th scope="col">Lieu d'arrivée</th>
                    <th scope="col">État livraison</th>
                </tr>
                </thead>
                <tbody>
                <struts:iterator value="livraisons">
                    <tr>
                        <th scope="row"><struts:property value="conducteur.nom"/> <struts:property
                                value="conducteur.prenom"/></th>
                        <th><struts:property value="camion.immatriculation"/></th>
                        <th><struts:property value="remorque.immatriculation"/></th>
                        <th><struts:property value="dateDepart"/></th>
                        <th><struts:property value="dateArrivee"/></th>
                        <th><struts:property value="sousCommande.lieuDepart.ville"/></th>
                        <th><struts:property value="sousCommande.lieuArrivee.ville"/></th>
                        <th><struts:property value="etatLivraison"/></th>
                    </tr>
                </struts:iterator>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
