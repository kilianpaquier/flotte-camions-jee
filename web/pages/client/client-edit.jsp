<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Profil client</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-75">
        <struts:form method="POST" action="updateClient" theme="simple" cssClass="card-body">
            <h3 class="card-title">Informations du profil client</h3>

            <!-- Client -->
            <h5 class="card-subtitle mt-2">Informations personnelles</h5>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="prenom">Prénom *</label>
                    <struts:textfield id="prenom" name="client.prenom" cssClass="form-control"
                                      value="%{client.prenom}" placeholder="Saisissez votre prénom" required="true"/>
                    <struts:fielderror fieldName="client.prenom" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="nom">Nom *</label>
                    <struts:textfield id="nom" name="client.nom" cssClass="form-control"
                                      value="%{client.nom}" placeholder="Saisissez votre nom" required="true"/>
                    <struts:fielderror fieldName="client.nom" cssClass="errorMessage"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="entreprise">Entreprise</label>
                    <struts:textfield id="entreprise" name="client.entreprise" cssClass="form-control"
                                      value="%{client.entreprise}" placeholder="Saisissez le nom de votre entreprise"/>
                    <struts:fielderror fieldName="client.entreprise" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="telephone">Téléphone *</label>
                    <struts:textfield id="telephone" name="client.telephone" cssClass="form-control"
                                      value="%{client.telephone}" placeholder="Saisissez votre numéro de téléphone"
                                      required="true"/>
                    <struts:fielderror fieldName="client.telephone" cssClass="errorMessage"/>
                </div>
            </div>

            <!-- Adresse -->
            <h5 class="card-subtitle mt-2">Adresse</h5>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="rue">Rue *</label>
                    <struts:textfield id="rue" name="client.adresse.rue" cssClass="form-control"
                                      value="%{client.adresse.rue}" placeholder="Saisissez l'adresse" required="true"/>
                    <struts:fielderror fieldName="client.adresse.rue" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="ville">Ville *</label>
                    <struts:textfield id="ville" name="client.adresse.ville" cssClass="form-control"
                                      value="%{client.adresse.ville}" placeholder="Saisissez la ville" required="true"/>
                    <struts:fielderror fieldName="client.adresse.ville" cssClass="errorMessage"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="pays">Pays *</label>
                    <struts:textfield id="pays" name="client.adresse.pays" cssClass="form-control"
                                      value="%{client.adresse.pays}" placeholder="Saisissez le pays"/>
                    <struts:fielderror fieldName="client.adresse.pays" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="codePostal">Code postal *</label>
                    <struts:textfield id="codePostal" name="client.adresse.codePostal" cssClass="form-control"
                                      value="%{client.adresse.codePostal}" placeholder="Saisissez votre code postal"
                                      required="true"/>
                    <struts:fielderror fieldName="client.adresse.codePostal" cssClass="errorMessage"/>
                </div>
            </div>

            <!-- Compte -->
            <h5 class="card-subtitle mt-2">Compte utilisateur</h5>
            <div class="row">
                <div class="form-group col-12 col-lg-6">
                    <label for="email">Adresse mail *</label>
                    <struts:textfield id="email" name="client.compte.email" cssClass="form-control"
                                      value="%{client.compte.email}" placeholder="Saisissez votre adresse mail"/>
                    <struts:fielderror fieldName="client.compte.email" cssClass="errorMessage"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="motDePasse">Mot de passe *</label>
                    <struts:password id="motDePasse" name="client.compte.motDePasse" cssClass="form-control"
                                     value="%{client.compte.motDePasse}" placeholder="Saisissez votre mot de passe"
                                     required="true"/>
                    <struts:fielderror fieldName="client.compte.motDePasse" cssClass="errorMessage"/>
                </div>
            </div>

            <!-- Messages & boutons -->
            <div class="form-group">
                <struts:actionmessage cssClass="infoMessage"/>
                <struts:actionerror cssClass="errorMessage"/>
                <struts:submit cssClass="btn btn-primary" value="Mettre à jour mon compte"/>
            </div>

        </struts:form>

        <struts:form method="POST" action="deleteClient" theme="simple" cssClass="card-body">
            <h3 class="card-title">Supprimer mon compte</h3>
            <!-- Messages & boutons -->
            <div class="form-group">
                <struts:actionmessage cssClass="infoMessage"/>
                <struts:actionerror cssClass="errorMessage"/>
                <struts:submit cssClass="btn btn-secondary" value="Supprimer mon compte"/>
            </div>
        </struts:form>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
