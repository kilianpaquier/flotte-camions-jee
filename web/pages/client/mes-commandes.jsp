<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Mes commandes</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h4 class="card-title text-center">Liste des commandes</h4>
            <table class="table mt-2 overflow-auto" style="max-height: 600px">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Id commande</th>
                    <th scope="col">Date de commande</th>
                    <th scope="col">Date de départ souhaitée</th>
                    <th scope="col">Lieu de départ</th>
                    <th scope="col">Lieu d'arrivée</th>
                </tr>
                </thead>
                <tbody>
                <struts:iterator value="commandes">
                    <struts:iterator value="sousCommandes">
                        <tr>
                            <th scope="row"><struts:property value="idCommande"/></th>
                            <th><struts:property value="dateCommande"/></th>
                            <th><struts:property value="dateDepart"/></th>
                            <th><struts:property value="lieuDepart.ville"/></th>
                            <th><struts:property value="lieuArrivee.ville"/></th>
                        </tr>
                    </struts:iterator>
                </struts:iterator>
                </tbody>
            </table>
            <h4 class="card-subtitle text-center mt-4">Mes livraisons en cours</h4>
            <table class="table mt-2 overflow-auto" style="max-height: 600px">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Id livraison</th>
                    <th scope="col">Date de départ</th>
                    <th scope="col">Date d'arrivée</th>
                    <th scope="col">Lieu de départ</th>
                    <th scope="col">Lieu d'arrivée</th>
                    <th scope="col">État livraison</th>
                </tr>
                </thead>
                <tbody>
                <struts:iterator value="livraisons">
                    <tr>
                        <th scope="row"><struts:property value="idLivraison"/></th>
                        <th><struts:property value="dateDepart"/></th>
                        <th><struts:property value="dateArrivee"/></th>
                        <th><struts:property value="sousCommande.lieuDepart.ville"/></th>
                        <th><struts:property value="sousCommande.lieuArrivee.ville"/></th>
                        <th><struts:property value="etatLivraison"/></th>
                    </tr>
                </struts:iterator>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
