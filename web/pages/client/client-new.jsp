<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Enregistrement</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-75">
        <struts:form method="POST" action="createClient" theme="simple" cssClass="card-body">
            <h3 class="card-title">Enregistrement</h3>
            <!-- Client -->
            <h5 class="card-subtitle mt-2">Informations personnelles</h5>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="firstname">Prénom *</label>
                    <struts:textfield id="firstname" name="client.prenom" cssClass="form-control"
                                      placeholder="Saisissez votre prénom" required="true"/>
                    <struts:fielderror fieldName="client.prenom" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="lastname">Nom *</label>
                    <struts:textfield id="lastname" name="client.nom" cssClass="form-control"
                                      placeholder="Saisissez votre nom" required="true"/>
                    <struts:fielderror fieldName="client.nom" cssClass="errorMessage"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="entreprise">Entreprise</label>
                    <struts:textfield id="entreprise" name="client.entreprise" cssClass="form-control"
                                      placeholder="Saisissez le nom de votre entreprise"/>
                    <struts:fielderror fieldName="client.entreprise" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="telephone">Téléphone *</label>
                    <struts:textfield id="telephone" name="client.telephone" cssClass="form-control"
                                      placeholder="Saisissez votre numéro de téléphone" required="true"/>
                    <struts:fielderror fieldName="client.telephone" cssClass="errorMessage"/>
                </div>
            </div>

            <!-- Adresse -->
            <h5 class="card-subtitle mt-2">Adresse</h5>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="rue">Rue *</label>
                    <struts:textfield id="rue" name="client.adresse.rue" cssClass="form-control"
                                      placeholder="Saisissez l'adresse" required="true"/>
                    <struts:fielderror fieldName="client.adresse.rue" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="ville">Ville *</label>
                    <struts:textfield id="ville" name="client.adresse.ville" cssClass="form-control"
                                      placeholder="Saisissez la ville" required="true"/>
                    <struts:fielderror fieldName="client.adresse.ville" cssClass="errorMessage"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="pays">Pays *</label>
                    <struts:textfield id="pays" name="client.adresse.pays" cssClass="form-control"
                                      placeholder="Saisissez le pays"/>
                    <struts:fielderror fieldName="client.adresse.pays" cssClass="errorMessage"/>
                </div>
                <div class="form-group col-md-12 col-lg-6">
                    <label for="codePostal">Code postal *</label>
                    <struts:textfield id="codePostal" name="client.adresse.codePostal" cssClass="form-control"
                                      placeholder="Saisissez votre code postal" required="true"/>
                    <struts:fielderror fieldName="client.adresse.codePostal" cssClass="errorMessage"/>
                </div>
            </div>

            <!-- Compte utilisateur -->
            <h5 class="card-subtitle mt-2">Compte utilisateur</h5>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="email">Adresse mail *</label>
                    <struts:textfield id="email" name="client.compte.email" cssClass="form-control"
                                      placeholder="Saisissez votre adresse mail" type="email"
                                      pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"/>
                    <struts:fielderror fieldName="client.compte.email" cssClass="errorMessage"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-lg-6">
                    <label for="password">Mot de passe *</label>
                    <struts:password id="password" name="client.compte.motDePasse" cssClass="form-control"
                                     placeholder="Saisissez votre mot de passe" required="true"/>
                    <struts:fielderror fieldName="client.compte.motDePasse" cssClass="errorMessage"/>
                </div>
            </div>

            <!-- Messages -->
            <div class="form-group">
                <struts:actionmessage/>
                <struts:actionerror/>
                <struts:submit cssClass="btn btn-primary" value="S'enregistrer"/>
            </div>

        </struts:form>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
