<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Liste des commandes</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h3 class="card-title text-center">Liste des commandes</h3>
            <table class="table mt-2 overflow-auto" style="max-height: 600px">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Client</th>
                    <th scope="col">Date de commande</th>
                    <th scope="col">Date de départ souhaitée</th>
                    <th scope="col">Lieu de départ</th>
                    <th scope="col">Lieu d'arrivée</th>
                    <th scope="col">#</th>
                </tr>
                </thead>
                <tbody>
                <struts:iterator value="commandes">
                    <struts:iterator value="sousCommandes">
                        <struts:form method="GET" theme="simple" action="validation-order">
                            <tr>
                                <struts:hidden name="idSousCommande" value="%{idSousCommande}"/>
                                <th scope="row"><struts:property value="client.nom"/> <struts:property
                                        value="client.prenom"/></th>
                                <th><struts:property value="dateCommande"/></th>
                                <th><struts:property value="dateDepart"/></th>
                                <th><struts:property value="lieuDepart.ville"/></th>
                                <th><struts:property value="lieuArrivee.ville"/></th>
                                <th><struts:submit cssClass="btn-sm btn-primary" value="Valider"/></th>
                            </tr>
                        </struts:form>
                    </struts:iterator>
                </struts:iterator>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
