<%--
  Created by IntelliJ IDEA.
  User: Charles
  Date: 18/10/2019
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Création commande</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container-fluid mt-5 py-5">
    <div class="card mx-auto">
        <div class="card-body">
            <h3 class="card-title text-center mb-4">Création de commande</h3>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3">
                    <label class="card-subtitle">Liste des marchandises de la sous commande</label>
                    <ul class="list-group mt-2 overflow-auto" style="max-height: 300px">
                        <jstl:if
                                test="${sessionScope.sousCommande == null}"> <%-- On regarde si une marchandise a déjà été ajoutée sur la sous commande --%>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Pas de marchandise enregistrée.
                            </li>
                        </jstl:if>
                        <jstl:forEach items="${sessionScope.sousCommande.marchandises}" var="marchandise">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <jstl:out value="${marchandise.nom}"/> - <jstl:out value="${marchandise.typeMarchandise}"/>
                                <div class="float-right">
                                        <span class="badge badge-primary badge-pill">
                                            <jstl:out value="${marchandise.volume}"/> m³
                                        </span>
                                </div>
                            </li>
                        </jstl:forEach>
                    </ul>
                    <div class="mt-2">
                        <struts:fielderror cssClass="errorMessage mb-1" name="remove.marchandise"/>
                        <input type="button" value="Ajouter une marchandise" class="btn btn-primary float-right"
                               data-toggle="modal"
                               data-target="#modalMarchandise"/>
                    </div>
                </div>
                <struts:form method="POST" action="add-order" theme="simple"
                             cssClass="col-sm-12 col-md-8 col-lg-4 mt-sm-4 mt-4 mt-lg-0 mt-md-0">
                    <h5 class="card-subtitle">Information sur la sous commande</h5>
                    <div class="form-group">
                        <label for="date">Date de départ souhaitée *</label>
                        <struts:textfield id="date" type="date" name="sousCommande.dateDepart"
                                          cssClass="form-control"
                                          placeholder="Saisissez la date" required="true"/>
                        <struts:fielderror fieldName="sousCommande.dateDepart" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="form-group">
                        <label for="rued">Lieu de départ *</label>
                        <struts:textfield id="rued" name="sousCommande.lieuDepart.rue" cssClass="form-control"
                                          placeholder="Adresse de départ" required="true"/>
                        <struts:fielderror fieldName="sousCommande.lieuDepart.rue" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="row">
                        <div class="form-group col-6 col-md-4">
                            <struts:textfield name="sousCommande.lieuDepart.ville" cssClass="form-control"
                                              placeholder="Ville de départ" required="true"/>
                            <struts:fielderror fieldName="sousCommande.lieuDepart.ville" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-6 col-md-4">
                            <struts:textfield name="sousCommande.lieuDepart.codePostal" cssClass="form-control"
                                              placeholder="Code postal" required="true"/>
                            <struts:fielderror fieldName="sousCommande.lieuDepart.codePostal"
                                               cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-6 col-md-4">
                            <struts:textfield name="sousCommande.lieuDepart.pays" cssClass="form-control"
                                              placeholder="Pays" required="true"/>
                            <struts:fielderror fieldName="sousCommande.lieuDepart.pays" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ruea">Lieu d'arrivée *</label>
                        <struts:textfield id="ruea" name="sousCommande.lieuArrivee.rue" cssClass="form-control"
                                          placeholder="Adresse d'arrivée" required="true"/>
                        <struts:fielderror fieldName="sousCommande.lieuArrivee.rue"
                                           cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="row">
                        <div class="form-group col-6 col-md-4">
                            <struts:textfield name="sousCommande.lieuArrivee.ville" cssClass="form-control"
                                              placeholder="Ville d'arrivée" required="true"/>
                            <struts:fielderror fieldName="sousCommande.lieuArrivee.ville"
                                               cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-6 col-md-4">
                            <struts:textfield name="sousCommande.lieuArrivee.codePostal" cssClass="form-control"
                                              placeholder="Code postal" required="true"/>
                            <struts:fielderror fieldName="sousCommande.lieuArrivee.codePostal"
                                               cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-6 col-md-4">
                            <struts:textfield name="sousCommande.lieuArrivee.pays" cssClass="form-control"
                                              placeholder="Pays" required="true"/>
                            <struts:fielderror fieldName="sousCommande.lieuArrivee.pays" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="frequence">Fréquence de livraison *</label>
                        <struts:select id="frequence" name="frequence" cssClass="custom-select"
                                       placeholder="Choisissez la fréquence de livraison" list="frequences"
                                       required="true"/>
                        <struts:fielderror fieldName="frequence" cssClass="errorMessage mb-n2"/>
                    </div>
                    <div class="">
                        <struts:actionerror cssClass="errorMessage mb-2 mt-n2"/>
                        <struts:submit cssClass="btn btn-primary mt-n1 float-right"
                                       value="Enregistrer la sous commande"/>
                    </div>
                </struts:form>
                <struts:form method="POST" action="submit-order"
                             cssClass="offset-lg-0 offset-md-4 col-md-8 col-lg-5 mt-md-4 mt-lg-0 mt-sm-4 mt-4"
                             theme="simple">
                    <h5 class="card-subtitle">Informations de la commande</h5>
                    <div class="form-group">
                        <label for="client">Identifiant client</label>
                        <struts:textfield id="client" name="commande.client.idClient" cssClass="form-control"
                                          placeholder="Identifiant du client" readonly="true"/>
                    </div>
                    <label class="card-subtitle">Liste des sous commandes</label>
                    <ul class="list-group mt-2 overflow-auto" style="max-height: 300px">
                        <jstl:if test="${sessionScope.commande == null}">
                            <%-- On regarde si une sous commande a déjà été faite sur cette commande --%>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Pas de sous commande enregistrée.
                            </li>
                        </jstl:if>
                        <jstl:forEach var="sousCommande" items="${sessionScope.commande.sousCommandes}">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Le <jstl:out value="${sousCommande.formatDateDepartJJMMYYYY()}"/> : De <jstl:out
                                    value="${sousCommande.lieuDepart.ville}"/>
                                vers
                                <jstl:out value="${sousCommande.lieuArrivee.ville}"/>
                                <span class="badge badge-primary badge-pill"><jstl:out
                                        value="${sousCommande.countMarchandises()}"/> m³</span>
                            </li>
                        </jstl:forEach>
                    </ul>
                    <div class="">
                        <struts:actionmessage cssClass="infoMessage mb-n1 mt-1"/>
                        <struts:actionerror cssClass="errorMessage mb-n1 mt-1"/>
                        <struts:submit cssClass="btn btn-primary mt-2 float-right ml-1" value="Passer la commande"/>
                        <struts:submit cssClass="btn btn-secondary mt-2 float-right" value="Supprimer la commande"
                                       action="delete-order"/>
                    </div>
                </struts:form>
            </div>
        </div>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
<%-- Pour gérer l'ajout de marchandise à la liste des marchandises d'une sous commande --%>
<script type="text/javascript" src="${cp}/resources/js/order.js"></script>
</body>

<!-- Modal -->
<div class="modal fade" id="modalMarchandise" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Information sur la marchandise</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <struts:form method="POST" action="add-marchandise" theme="simple">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="marchandise">Marchandise</label>
                            <struts:textfield id="marchandise" name="marchandise.nom" cssClass="form-control"
                                              list="marchandisesList"
                                              placeholder="Nom de la marchandise" required="true"/>
                            <struts:fielderror fieldName="marchandise.nom" cssClass="errorMessage mb-n2"/>
                                <%--<datalist id="marchandisesList">
                                    <struts:iterator var="marchandisesList" value="marchandisesList">
                                        <option><struts:property value="nom"/></option>
                                    </struts:iterator>
                                </datalist>--%>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="volume">Volume transporté (m³)</label>
                            <struts:textfield type="number" name="marchandise.volume" min="1" value="1" id="volume"
                                              cssClass="form-control" required="true"/>
                            <struts:fielderror fieldName="marchandise.volume" cssClass="errorMessage mb-n2"/>
                        </div>
                        <div class="form-group col-md-12 col-lg-6">
                            <label for="typeMarchandise">Type de marchandise</label>
                            <struts:select id="typeMarchandise" name="typeMarchandise" cssClass="custom-select"
                                           list="typeMarchandises" required="true"/>
                            <struts:fielderror fieldName="typeMarchandise" cssClass="errorMessage mb-n2"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <struts:submit cssClass="btn btn-primary" value="Ajouter la marchandise"/>
                </div>
            </struts:form>
        </div>
    </div>
</div>
</html>
