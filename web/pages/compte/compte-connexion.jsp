<%--
  Created by IntelliJ IDEA.
  User: Kilian
  Date: 19/10/2019
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="struts" uri="/struts-tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <title>Connexion</title>
    <%@ include file="/pages/template/head.jsp" %>
</head>
<body>
<%@ include file="/pages/template/nav.jsp" %>
<div class="container mt-5 py-5">
    <div class="card mx-auto w-50">
        <struts:form method="POST" action="connexion" theme="simple" cssClass="card-body">
            <h3 class="card-title">Se connecter</h3>
            <div class="form-group">
                <label for="email">Adresse mail *</label>
                <struts:textfield name="compte.email" type="email" id="email" cssClass="form-control"
                                  placeholder="Saisissez votre adresse mail" required="true"
                                  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"/>
                <struts:fielderror fieldName="compte.email" cssClass="errorMessage"/>
            </div>
            <div class="form-group">
                <label for="password">Mot de passe *</label>
                <struts:password name="compte.motDePasse" id="password" cssClass="form-control"
                                 placeholder="Saisissez votre mot de passe" required="true"/>
                <struts:fielderror fieldName="compte.motDePasse" cssClass="errorMessage"/>
            </div>
            <div class="form-group">
                <struts:actionmessage/>
                <struts:actionerror/>
                <struts:submit cssClass="btn btn-primary float-right" value="Se connecter"/>
            </div>
        </struts:form>
    </div>
</div>
<%@ include file="/pages/template/footer.jsp" %>
</body>
</html>
