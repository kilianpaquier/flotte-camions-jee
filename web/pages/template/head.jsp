<%--
  Created by IntelliJ IDEA.
  User: Kilian
  Date: 19/10/2019
  Time: 12:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="${cp}/resources/images/logo_archi_1.jpg">
<link rel="stylesheet" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="${cp}/resources/css/style.css">
