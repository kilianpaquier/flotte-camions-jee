<%--
  Created by IntelliJ IDEA.
  User: Kilian
  Date: 19/10/2019
  Time: 12:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<jstl:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<struts:set var="typeCompteSession" value="#session.containsKey('typeCompte')?#session.typeCompte.toString():null"/>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <a class="navbar-brand" href="index.action">
            <span class="logo">
                <img src="${cp}/resources/images/logo_archi_1.jpg" width="30" height="30" alt="logo"/>
            </span>
            <span class="px-1">Accueil</span>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <!--   -->
                <struts:if test="%{#typeCompteSession != null && #typeCompteSession.equals('Opérateur')}">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdownGestion" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Gestion
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownGestion">
                            <a class="dropdown-item" href="employe-list.action">Employés</a>
                            <a class="dropdown-item" href="list-camion.action">Camions</a>
                            <a class="dropdown-item" href="remorques.action">Remorques</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="order-list.action">Liste des commandes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="delivery-list.action">Livraisons en cours</a>
                    </li>
                </struts:if>
                <struts:elseif test="%{#typeCompteSession != null && #typeCompteSession.equals('Client')}">
                    <li class="nav-item">
                        <a class="nav-link" href="order.action">Passer une commande</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="mes-commandes.action">Mes commandes</a>
                    </li>
                </struts:elseif>
            </ul>
            <ul class="navbar-nav">
                <struts:if test="%{#session.containsKey('clientID')}">
                    <li class="nav-item">
                        <a class="nav-link" href="profilClient.action">Profil</a>
                    </li>
                </struts:if>
                <struts:if test="%{#session.containsKey('compteID')}">
                    <li class="nav-item">
                        <a class="nav-link" href="deconnexion.action">Déconnexion</a>
                    </li>
                </struts:if>
                <struts:elseif test="%{#session.size == 0}">
                    <li class="nav-item">
                        <a class="nav-link" href="inscriptionClient.action">Inscription</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="connexionIndex.action">Connexion</a>
                    </li>
                </struts:elseif>
            </ul>
        </div>
    </nav>
</header>
